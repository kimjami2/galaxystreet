<?php
/*
Template Name: Contact Template
*/
?>

<div id="main-content" class="contact-page">
	<section class="page-section primary centered narrow page-header">
	  <div class="container-fluid">
	<?php
	  if(has_post_thumbnail()) {
	?>
	    <div class="row head parallax-window background" data-parallax="scroll" data-image-src="<?PHP echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large') ?>">
	<?php
	} else {
	?>
	    <div class="row head">
	<?php
	}
	?>
	      <div class="overlay">
	        <div class="text">
	          <div class="col-md-6 col-md-push-3" style="text-align: center;">
	            <h2><?php echo roots_title(); ?></h2>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-push-3">
				<form class="clearfix contact-form">
	        <div class="row">
	          <div class="input-field col-md-12 col-sm-12">
	            <input id="first-name" type="text" class="validate" required>
	            <label for="first-name">First Name</label>
	          </div>
	        </div>
	        <div class="row">
	          <div class="input-field col-md-12 col-sm-12">
	            <input id="last-name" type="text" class="validate" required>
	            <label for="last-name" class="brand-primary-color">Last Name</label>
	          </div>
	        </div>
	        <div class="row">
	          <div class="input-field col-md-12 col-sm-12">
	            <input id="email" type="email" class="validate" required>
	            <label for="email">Email</label>
	          </div>
	        </div>
	        <div class="row">
	          <div class="input-field col-md-12 col-sm-12">
	            <textarea id="message" class="materialize-textarea" required></textarea>
	            <label for="message">Text</label>
	          </div>
	        </div>
					<div class="row send-container">
						<div class="mail-sent-conf"><?PHP _e('The mail has been sent!', 'galaxystreet'); ?></div>
	          <btn class="btn btn-primary send-contact-email"><?PHP _e('Send', 'galaxystreet'); ?></btn>
						<input type="submit" style="display:none;"/>
	        </div>
				</form>
			</div>
		</div>
	</div>
	<div class="container-fluid contact-info">
			<div class="contact">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-sm-6 col-md-3">
								<div class="row">
									<div class="col-xs-4 col-sm-3 col-md-4"><p class="icon brand-primary-bg"><i class="fa fa-user"></i></p></div>
									<div class="col-xs-8 col-sm-9 col-md-8 contact-text"><h3><?PHP _e('name', 'galaxystreet'); ?></h3><?PHP echo atomic_option('atomic_agent_name'); ?></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="row">
									<div class="col-xs-4 col-sm-3 col-md-4"><p class="icon brand-primary-bg"><i class="fa fa-envelope"></i></p></div>
									<div class="col-xs-8 col-sm-9 col-md-8 contact-text"><h3><?PHP _e('Email', 'galaxystreet'); ?></h3><?PHP echo atomic_option('atomic_agent_email'); ?></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="row">
									<div class="col-xs-4 col-sm-3 col-md-4"><p class="icon brand-primary-bg"><i class="fa fa-phone"></i></p></div>
									<div class="col-xs-8 col-sm-9 col-md-8 contact-text"><h3><?PHP _e('Phone', 'galaxystreet'); ?></h3><?PHP echo atomic_option('atomic_agent_phone'); ?></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="row">
									<div class="col-xs-4 col-sm-3 col-md-4"><p class="icon brand-primary-bg"><i class="fa fa-globe"></i></p></div>
									<div class="col-xs-8 col-sm-9 col-md-8 contact-text"><h3><?PHP _e('Website', 'galaxystreet'); ?></h3><?PHP echo atomic_option('atomic_agent_website'); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="container-fluid maps">
		<?PHP echo do_shortcode('[google-maps]Planetgatan 37C Halmstad Sverige[/google-maps]'); ?>
	</div>
</div>
