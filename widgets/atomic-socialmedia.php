<?PHP
class wpb_widget extends WP_Widget {

	function __construct() {
			parent::__construct(
			// widget ID
			'atomic_widget',

			// widget title
			__('Atomic Socialmedia', 'wpb_widget_domain'),

			// widget description
			array( 'description' => __( 'Social media icons and links', 'wpb_widget_domain' ), )
		);
	}

	// admin widget
	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );

		if(isset($instance['facebook_url']) && !empty($instance['facebook_url'])) {
			$facebook = '<a class="icon-button facebook" href="'.$instance['facebook_url'].'" target="new"><i class="mdi mdi-facebook"></i><span></span></a>';
		}
		if(isset($instance['twitter_url']) && !empty($instance['twitter_url'])) {
			$twitter = '<a class="icon-button twitter" href="'.$instance['twitter_url'].'" target="new"><i class="mdi mdi-twitter"></i><span></span></a>';
		}
		if(isset($instance['tumblr_url']) && !empty($instance['tumblr_url'])) {
			$tumblr = '<a class="icon-button tumblr" href="'.$instance['tumblr_url'].'" target="new"><i class="fa fa-tumblr"></i><span></span></a>';
		}
		if(isset($instance['googleplus_url']) && !empty($instance['googleplus_url'])) {
			$googleplus = '<a class="icon-button google-plus" href="'.$instance['googleplus_url'].'" target="new"><i class="mdi mdi-google-plus"></i><span></span></a>';
		}
		if(isset($instance['vimeo_url']) && !empty($instance['vimeo_url'])) {
			$vimeo = '<a class="icon-button vimeo" href="'.$instance['vimeo_url'].'" target="new"><i class="mdi mdi-vimeo"></i><span></span></a>';
		}
		if(isset($instance['youtube_url']) && !empty($instance['youtube_url'])) {
			$youtube = '<a class="icon-button youtube" href="'.$instance['youtube_url'].'" target="new"><i class="mdi mdi-youtube-play"></i><span></span></a>';
		}
		if(isset($instance['pinterest_url']) && !empty($instance['pinterest_url'])) {
			$pinterest = '<a class="icon-button pinterest" href="'.$instance['pinterest_url'].'" target="new"><i class="mdi mdi-pinterest"></i><span></span></a>';
		}

		echo '
			<section class="widget widget-atomic-social">
			<h3>'.$title.'</h2>
				'.$facebook.'
				'.$twitter.'
				'.$tumblr.'
				'.$googleplus.'
				'.$vimeo.'
				'.$youtube.'
				'.$pinterest.'
			</section>
		';
	}

	// Widget Backend
	public function form( $instance ) {
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook_url' ); ?>"><?php _e( 'Facebook URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'facebook_url' ); ?>" name="<?php echo $this->get_field_name( 'facebook_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['facebook_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter_url' ); ?>"><?php _e( 'Twitter URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'twitter_url' ); ?>" name="<?php echo $this->get_field_name( 'twitter_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['twitter_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tumblr_url' ); ?>"><?php _e( 'Tumblr URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'tumblr_url' ); ?>" name="<?php echo $this->get_field_name( 'tumblr_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['tumblr_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'googleplus_url' ); ?>"><?php _e( 'Google Plus URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'googleplus_url' ); ?>" name="<?php echo $this->get_field_name( 'googleplus_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['googleplus_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'vimeo_url' ); ?>"><?php _e( 'Vimeo URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'vimeo_url' ); ?>" name="<?php echo $this->get_field_name( 'vimeo_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['vimeo_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube_url' ); ?>"><?php _e( 'Youtube URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'youtube_url' ); ?>" name="<?php echo $this->get_field_name( 'youtube_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['youtube_url'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'pinterest_url' ); ?>"><?php _e( 'Pinterest URL:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest_url' ); ?>" name="<?php echo $this->get_field_name( 'pinterest_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['pinterest_url'] ); ?>" />
		</p>
	<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title']				= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['facebook_url'] 		= ( ! empty( $new_instance['facebook_url'] ) ) ? strip_tags( $new_instance['facebook_url'] ) : '';
		$instance['twitter_url'] 		= ( ! empty( $new_instance['twitter_url'] ) ) ? strip_tags( $new_instance['twitter_url'] ) : '';
		$instance['tumblr_url'] 		= ( ! empty( $new_instance['tumblr_url'] ) ) ? strip_tags( $new_instance['tumblr_url'] ) : '';
		$instance['googleplus_url'] 	= ( ! empty( $new_instance['googleplus_url'] ) ) ? strip_tags( $new_instance['googleplus_url'] ) : '';
		$instance['vimeo_url'] 			= ( ! empty( $new_instance['vimeo_url'] ) ) ? strip_tags( $new_instance['vimeo_url'] ) : '';
		$instance['youtube_url'] 		= ( ! empty( $new_instance['youtube_url'] ) ) ? strip_tags( $new_instance['youtube_url'] ) : '';
		$instance['pinterest_url'] 	= ( ! empty( $new_instance['pinterest_url'] ) ) ? strip_tags( $new_instance['pinterest_url'] ) : '';

		return $instance;
	}
}

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
?>
