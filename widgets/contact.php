<?PHP
class wpb_ap_contact extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_contact',

      // widget title
      __('GalaxyStreet Contact', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP Contact', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );

      // add media upload scripts
      add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
   }

   public function upload_scripts()
   {
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

      wp_enqueue_style('thickbox');
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );

      /*
      * HTML
      */
      $html = '
      <div class="gs-contact gs-section container-fluid">
      <div class="container">
      <div class="row">
      <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3">
      <h2>Contact the agent</h2><div class="line"></div>
      <p class="desc">
      Ask questions or book a meeting with the agent for futher information about the estate
      </p>
      </div>
      </div>
      <div class="row">
      <form class="col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 clearfix contact-form">
      <div class="row">
      <div class="input-field col-md-12 col-sm-12">
      <input id="first-name" type="text" class="validate" required>
      <label for="first-name">First Name</label>
      </div>
      </div>
      <div class="row">
      <div class="input-field col-md-12 col-sm-12">
      <input id="last-name" type="text" class="validate" required>
      <label for="last-name">Last Name</label>
      </div>
      </div>
      <div class="row">
      <div class="input-field col-md-12 col-sm-12">
      <input id="email" type="email" class="validate" required>
      <label for="email">Email</label>
      </div>
      </div>
      <div class="row">
      <div class="input-field col-md-12 col-sm-12">
      <textarea id="message" class="materialize-textarea" required></textarea>
      <label for="message">Text</label>
      </div>
      </div>
      <div class="row send-container">
      <div class="mail-sent-conf">'.__('The mail has been sent!', 'galaxystreet').'</div>
      <btn class="btn btn-primary send-contact-email">'.__('Send', 'galaxystreet').'</btn>
      <input type="submit" style="display:none;"/>
      </div>
      </form>
      </div>
      </div>
      </div>
      ';

      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('descripion'); ?>"><?php _e('Description'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('descripion'); ?>" type="text" value="<?php echo esc_attr($instance['descripion']); ?>" />
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']	      = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description']   = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_contact() {
   register_widget('wpb_ap_contact');
}
add_action('widgets_init', 'wpb_load_ap_contact');
?>
