<?PHP
class wpb_ap_title_description extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_title_description',

      // widget title
      __('GalaxyStreet Title & Description', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP Title & Description', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );

      // add media upload scripts
      add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
   }

   public function upload_scripts()
   {
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

      wp_enqueue_style('thickbox');
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );

      /*
      * HTML
      */
      $center = ($instance['center'] == 'on') ? 'center' : '';

      $html = '<div class="title-desc '.$center.'">';

      if(!empty($instance['title'])) {
        $html .= '<h2>'.$instance['title'].'</h2><div class="line"></div>';
      }

      if(!empty($instance['description'])) {
        $html .= '<p>'.$instance['description'].'</p>';
      }

      $html .= '</div>';
      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('descripion'); ?>"><?php _e('Description'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text" value="<?php echo esc_attr($instance['description']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('center'); ?>"><?php _e('Center the content'); ?></label>
         <input class="checkbox" type="checkbox" <?php checked($instance['center'], 'on'); ?> id="<?php echo $this->get_field_id('center'); ?>" name="<?php echo $this->get_field_name('center'); ?>" />
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']	      = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description']   = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
      $instance['center']        = ( ! empty( $new_instance['center'] ) ) ? $new_instance['center'] : '';
      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_title_description() {
   register_widget('wpb_ap_title_description');
}
add_action('widgets_init', 'wpb_load_ap_title_description');
?>
