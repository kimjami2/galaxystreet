<?PHP
class wpb_ap_maps extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_maps',

      // widget title
      __('GalaxyStreet Google Maps', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP Maps', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );

      // add media upload scripts
      add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
   }

   public function upload_scripts()
   {
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

      wp_enqueue_style('thickbox');
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );
      $mapsId = 'google-maps-'.md5($instance['address'].time());
      $bgcolor = !empty($instance['bgcolor']) ? 'brand-background' : '';

      /*
      * HTML
      */
      $html = '
      <div class="home-maps gs-section '.$bgcolor.'">
      <div class="container">
      <div class="row">
      <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-10 col-sm-push-1">';

      $html .= !empty($instance['title']) ? '<h2>'.$instance['title'].'</h2><div class="line"></div>' : false;
      $html .= !empty($instance['description']) ? '<p class="desc">'.$instance['description'].'</p>' : false;

      $html .= '
      </div>
      </div>
      </div>';

      $html .= '
      <div id="'.$mapsId.'" class="google-maps"></div>

      <script type="text/javascript">
      $(document).ready(function() {
        $.ajax({
          url: "http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($instance['address']).'",
          type: "POST",
          success: function(res){
           var mapOptions = {
             zoom: 15,
             scrollwheel: false,
             draggable: false,
             center: new google.maps.LatLng(res.results[0].geometry.location.lat, res.results[0].geometry.location.lng),
             styles: [{elementType: "labels", stylers: [{ visibility: "off" }]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"color":"#e4eed0"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#639c35"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#fffad8"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f8f79a"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#2b5d18"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#fcfcfc"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#f7f7f4"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#e0e0e0"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#646464"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#f6e881"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#bbdcf1"}]}]
           };
           var mapElement = document.getElementById("'.$mapsId.'");
           var map = new google.maps.Map(mapElement, mapOptions);
           var marker = new google.maps.Marker({
             position: new google.maps.LatLng(res.results[0].geometry.location.lat, res.results[0].geometry.location.lng),
             map: map,
             title: ""
           });
          }
        });
      });
      </script>
      </div>
      ';

      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text" value="<?php echo esc_attr($instance['description']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo esc_attr($instance['address']); ?>" />
      </p>
      <p class="ap-widget-margin-top">
        <input class="checkbox" type="checkbox" <?php checked( $instance[ 'bgcolor' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" />
        <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>">Theme background color</label>
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']	      = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description']  = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
      $instance['address']      = ( ! empty( $new_instance['address'] ) ) ? $new_instance['address'] : '';
      $instance['bgcolor'] 		  = $new_instance['bgcolor'];
      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_maps() {
   register_widget('wpb_ap_maps');
}
add_action('widgets_init', 'wpb_load_ap_maps');
?>
