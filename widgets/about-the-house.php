<?PHP
class wpb_ap_about_the_house extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_about_the_house',

      // widget title
      __('GalaxyStreet About the House', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP About the house', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );

      // add media upload scripts
      add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
   }

   public function upload_scripts()
   {
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

      wp_enqueue_style('thickbox');
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );

      /*
      * HTML
      */
      $html = '
      <div class="home-about container-fluid gs-section image " style="background-image: url('.$instance['image'].');">
      <div class="row">
      <div class="info head col-lg-6 col-md-6 col-sm-12">
      <div class="wrapper">
      <div class="inner-wrapper">
      ';


      if(!empty($instance['title'])) {
         $html .= '<h2>'.$instance['title'].'</h2><!--<div class="line"></div>-->';
      }

      if(!empty($instance['description'])) {
         $html .= '<p class="desc">'.nl2br($instance['description']).'</p>';
      }

      if(!empty($instance['infobox1']) || !empty($instance['infoboxtitle1'])) {
        $html .= '<div class="row additional">';
      }

      if(!empty($instance['infobox1']) || !empty($instance['infoboxtitle1'])) {
        $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
        $html .= !empty($instance['infoboxtitle1']) ? '<h3>'.$instance['infoboxtitle1'].'</h3>': '';
        $html .= !empty($instance['infobox1']) ? '<p>'.$instance['infobox1'].'</p>': '';
        $html .= '</div>';
      }

      if(!empty($instance['infobox2']) || !empty($instance['infoboxtitle2'])) {
        $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
        $html .= !empty($instance['infoboxtitle2']) ? '<h3>'.$instance['infoboxtitle2'].'</h3>': '';
        $html .= !empty($instance['infobox2']) ? '<p>'.$instance['infobox2'].'</p>': '';
        $html .= '</div>';
      }

      if(!empty($instance['infobox3']) || !empty($instance['infoboxtitle3'])) {
        $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
        $html .= !empty($instance['infoboxtitle3']) ? '<h3>'.$instance['infoboxtitle3'].'</h3>': '';
        $html .= !empty($instance['infobox3']) ? '<p>'.$instance['infobox3'].'</p>': '';
        $html .= '</div>';
      }

      if(!empty($instance['infobox4']) || !empty($instance['infoboxtitle4'])) {
        $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
        $html .= !empty($instance['infoboxtitle4']) ? '<h3>'.$instance['infoboxtitle4'].'</h3>': '';
        $html .= !empty($instance['infobox4']) ? '<p>'.$instance['infobox4'].'</p>': '';
        $html .= '</div>';
      }

      if(!empty($instance['infobox1']) || !empty($instance['infoboxtitle1'])) {
        $html .= '</div>';
      }

      if(!empty($instance['buttonText'])) {
         $html .= '
         <div class="read-more">
         <a id="home-about-btn" href="'.addhttp($instance['buttonURL']).'" class="btn btn-pure-white medium scroll-prepare">'.$instance['buttonText'].'</a>
         </div>';
      }

      $html .= '
      </div>
      </div>
      </div>
      </div>
      </div>
      ';

      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'infobox1' ); ?>"><?php _e('Info box 1'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('infoboxtitle1'); ?>" name="<?php echo $this->get_field_name('infoboxtitle1'); ?>" type="text" value="<?php echo esc_attr($instance['infoboxtitle1']); ?>" placeholder="Title.." />
         <textarea class="widefat" id="<?php echo $this->get_field_id('infobox1'); ?>" name="<?php echo $this->get_field_name('infobox1'); ?>" rows="6"><?php echo esc_attr( $instance['infobox1'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'infobox2' ); ?>"><?php _e('Info box 2'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('infoboxtitle2'); ?>" name="<?php echo $this->get_field_name('infoboxtitle2'); ?>" type="text" value="<?php echo esc_attr($instance['infoboxtitle2']); ?>" placeholder="Title.." />
         <textarea class="widefat" id="<?php echo $this->get_field_id('infobox2'); ?>" name="<?php echo $this->get_field_name('infobox2'); ?>" rows="6"><?php echo esc_attr( $instance['infobox2'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'infobox3' ); ?>"><?php _e('Info box 3'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('infoboxtitle3'); ?>" name="<?php echo $this->get_field_name('infoboxtitle3'); ?>" type="text" value="<?php echo esc_attr($instance['infoboxtitle3']); ?>" placeholder="Title.." />
         <textarea class="widefat" id="<?php echo $this->get_field_id('infobox3'); ?>" name="<?php echo $this->get_field_name('infobox3'); ?>" rows="6"><?php echo esc_attr( $instance['infobox3'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'infobox4' ); ?>"><?php _e('Info box 4'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('infoboxtitle4'); ?>" name="<?php echo $this->get_field_name('infoboxtitle4'); ?>" type="text" value="<?php echo esc_attr($instance['infoboxtitle4']); ?>" placeholder="Title.." />
         <textarea class="widefat" id="<?php echo $this->get_field_id('infobox4'); ?>" name="<?php echo $this->get_field_name('infobox4'); ?>" rows="6"><?php echo esc_attr( $instance['infobox4'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" style="width: 60%;" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo esc_attr($instance['image']); ?>" />
         <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('buttonText'); ?>"><?php _e('Button text'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('buttonText'); ?>" name="<?php echo $this->get_field_name('buttonText'); ?>" type="text" value="<?php echo esc_attr($instance['buttonText']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('buttonURL'); ?>"><?php _e('Button URL'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('buttonURL'); ?>" name="<?php echo $this->get_field_name('buttonURL'); ?>" type="text" value="<?php echo esc_attr($instance['buttonURL']); ?>" />
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']			= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description'] 	= ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';

      $instance['infobox1'] 	= ( ! empty( $new_instance['infobox1'] ) ) ? $new_instance['infobox1'] : '';
      $instance['infoboxtitle1'] 	= ( ! empty( $new_instance['infoboxtitle1'] ) ) ? $new_instance['infoboxtitle1'] : '';
      $instance['infobox2'] 	= ( ! empty( $new_instance['infobox2'] ) ) ? $new_instance['infobox2'] : '';
      $instance['infoboxtitle2'] 	= ( ! empty( $new_instance['infoboxtitle2'] ) ) ? $new_instance['infoboxtitle2'] : '';
      $instance['infobox3'] 	= ( ! empty( $new_instance['infobox3'] ) ) ? $new_instance['infobox3'] : '';
      $instance['infoboxtitle3'] 	= ( ! empty( $new_instance['infoboxtitle3'] ) ) ? $new_instance['infoboxtitle3'] : '';
      $instance['infobox4'] 	= ( ! empty( $new_instance['infobox4'] ) ) ? $new_instance['infobox4'] : '';
      $instance['infoboxtitle4'] 	= ( ! empty( $new_instance['infoboxtitle4'] ) ) ? $new_instance['infoboxtitle4'] : '';

      $instance['image'] 		   = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
      $instance['buttonText']    = ( ! empty( $new_instance['buttonText'] ) ) ? strip_tags( $new_instance['buttonText'] ) : '';
      $instance['buttonURL'] 		= ( ! empty( $new_instance['buttonURL'] ) ) ? strip_tags( $new_instance['buttonURL'] ) : '';

      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_about_the_house() {
   register_widget( 'wpb_ap_about_the_house' );
}
add_action( 'widgets_init', 'wpb_load_ap_about_the_house' );
?>
