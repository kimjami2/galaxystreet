<?PHP
class wpb_ap_about_the_agent extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_about_the_agent',

      // widget title
      __('GalaxyStreet About the Agent', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP About the agent', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );

      // add media upload scripts
      add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
   }

   public function upload_scripts()
   {
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

      wp_enqueue_style('thickbox');
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );
      $has_image = (isset($instance['image']) && !empty($instance['image']));

      /*
      * HTML
      */
      $html = '
      <div class="home-agent container gs-section">
      <div class="row">';

      if($has_image) {
        $html .= '
        <div class="col-image col-md-6 col-sm-12">
        <div class="image" style="background-image: url('.$instance['image'].');"></div>
        </div>
        <div class="info head col-md-6 col-sm-12 ">';
      } else {
        $html .= '
        <div class="info head col-md-6 col-md-push-3 col-sm-12 ">';
      }

      $html .= '
      <div class="wrapper">';

      $html .= !empty($instance['name']) ? '<h2>'.$instance['name'].'</h2><div class="line"></div>' : false;
      $html .= !empty($instance['title']) ? '<h4 class="title">'.$instance['title'].'</h4>' : false;
      $html .= !empty($instance['description']) ? '<p class="desc">'.nl2br($instance['description']).'</p>' : false;

      $html .= '
      <div class="contact">
      <div class="row">';

      if(!empty($instance['phone'])) {
         $html .= '<div class="col-md-6 col-sm-6 col-xs-12"><i class="fa fa-phone"></i><p>'.$instance['phone'].'</p></div>';
      }
      if(!empty($instance['email'])) {
         $html .= '<div class="col-md-6 col-sm-6 col-xs-12"><i class="fa fa-share"></i><p>'.$instance['email'].'</p></div>';
      }

      $html .= '
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      ';

      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description'); ?></label>
         <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" rows="6"><?php echo esc_attr( $instance['description'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" style="width: 60%;" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo esc_attr($instance['image']); ?>" />
         <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('name'); ?>"><?php _e('Agent name'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('name'); ?>" name="<?php echo $this->get_field_name('name'); ?>" type="text" value="<?php echo esc_attr($instance['name']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Agent email'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($instance['email']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Agent phone number'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($instance['phone']); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('website'); ?>"><?php _e('Agent website'); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id('website'); ?>" name="<?php echo $this->get_field_name('website'); ?>" type="text" value="<?php echo esc_attr($instance['website']); ?>" />
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']			   = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description'] 	 = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
      $instance['image'] 		     = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
      $instance['name']          = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
      $instance['email']         = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
      $instance['phone']         = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
      $instance['website']       = ( ! empty( $new_instance['website'] ) ) ? strip_tags( $new_instance['website'] ) : '';
      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_about_the_agent() {
   register_widget( 'wpb_ap_about_the_agent' );
}
add_action( 'widgets_init', 'wpb_load_ap_about_the_agent' );
?>
