<?PHP
class wpb_ap_gallery_shortcut extends WP_Widget {

  function __construct() {
    parent::__construct(
    // widget ID
    'ap_gallery_shortcut',

    // widget title
    __('GalaxyStreet Gallery Shortcut', 'wpb_widget_domain'),

    // widget params
    array(
      'description' => __( 'Gallery shortcut preview', 'wpb_widget_domain' ),
      'panels_groups' => array('galaxystreet'),
      'panels_icon' => 'ap-logo-icon'
      )
    );
  }

  // admin widget
  public function widget( $args, $instance ) {

    $title        = apply_filters( 'widget_title', $instance['title'] );
    $bgcolor      = !empty($instance['bgcolor']) ? 'brand-background' : '';
    $taxonomies   = array('gallery_categories');
    $args         = array('orderby' => 'name','order' => 'ASC','hide_empty' => true,);

    /*
    * HTML
    */

    $html = '<div class="home-gallery container-fluid gs-section '.$bgcolor.'">
    <div class="container-fluid">';

    // Title & Description
    if(!empty($instance['title']) || !empty($instance['description'])) {
      $html .= '
      <div class="row">
      <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-10 col-sm-push-1">';

      $html .= !empty($instance['title']) ? '<h2>'.$instance['title'].'</h2><div class="line"></div>' : false;
      $html .= !empty($instance['description']) ? '<p class="desc">'.$instance['description'].'</p>' : false;

      $html .= '
      </div>
      </div>';
    }

    // List all our images
    $html .= '<div class="row images">';

    $terms = get_terms($taxonomies, $args);
    foreach($terms as $key => $category) {

      $args = array(
        'post_type' => 'gallery_images',
        'gallery_categories' => $category->slug
      );

      $query = new WP_Query( $args );
      if($query->have_posts()) {
        $query->the_post();
        $countText = count($query->posts) > 0 ? 'Images' : 'Image';
        $html .= '
        <a href="'.$instance['url'].'?category='.$category->slug.'" class="preview col-lg-3 col-md-4 col-sm-6">
        <div class="col-md-12">
        <div class="overlay soft">
        <div class="icon brand-primary-color">
        <i class="fa fa-arrow-right"></i>
        </div>
        <div class="count">
        <p>'.count($query->posts).' '.$countText.'</p>
        </div>
        </div>
        <div class="title">
        <p>'.$category->name.'</p>
        </div>
        </div>
        <div class="image soft" style="background-image: url('. wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0] .');"></div>
        </a>
        ';

      }
      wp_reset_postdata();
    }

    $html .= '
    </div>
    <div class="row see-all-images clearfix">
    <a href="'.$a['galleryurl'].'" id="home-gallery-btn" class="btn btn-primary scroll-prepare">See all images</a>
    </div>
    </div>
    </div>
    ';

    echo $html;
  }

  // Widget Backend
  public function form( $instance ) {
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>" class="ap-widget-input-label"><?php _e( 'Title' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'description' ); ?>" class="ap-widget-input-label"><?php _e( 'Description' ); ?></label>
      <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $instance['description'] ); ?></textarea>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'url' ); ?>" class="ap-widget-input-label"><?php _e( 'URL' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $instance['url'] ); ?>" />
    </p>
    <p class="ap-widget-margin-top">
      <input class="checkbox" type="checkbox" <?php checked( $instance[ 'bgcolor' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" />
      <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>">Theme background color</label>
    </p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title']			= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['description'] 	= ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
    $instance['url'] 		      = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
    $instance['bgcolor'] 		  = $new_instance['bgcolor'];
    return $instance;
  }
}

// Register and load the widget
function wpb_load_ap_gallery_shortcut() {
  register_widget( 'wpb_ap_gallery_shortcut' );
}
add_action( 'widgets_init', 'wpb_load_ap_gallery_shortcut' );
?>
