<?PHP
class wpb_ap_testimonial extends WP_Widget {

  function __construct() {
    parent::__construct(
    // widget ID
    'ap_testimonial',

    // widget title
    __('GalaxyStreet Testimonial', 'wpb_widget_domain'),

    // widget params
    array(
      'description' => __( 'AP Testimonial', 'wpb_widget_domain' ),
      'panels_groups' => array('galaxystreet'),
      'panels_icon' => 'ap-logo-icon'
      )
    );

    // add media upload scripts
    add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
  }

  public function upload_scripts()
  {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

    wp_enqueue_style('thickbox');
  }

  // admin widget
  public function widget( $args, $instance ) {

    $title = apply_filters( 'widget_title', $instance['title'] );

    /*
    * HTML
    */
    $html = '
    <div class="home-testimonials parallax-window" data-parallax="scroll" data-image-src="'.$instance["backgroundImage"].'">
    <div class="container-fluid ani">

    <div class="home-testimonials-info row info scroll-prepare">
      <div class="testimonials-slide">';


    $args=array(
      'post_type' => 'testimonials',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'caller_get_posts'=> 1);

      $testimonials = new WP_Query($args);
      if($testimonials->have_posts()) {

        $testimonials->the_post();

        foreach($testimonials->posts as $post) {
          $meta = get_post_meta($post->ID);
          $html .= '
          <div class="slide row">
          <div class="caption-wrapper">
          <div class="info col-lg-6 col-lg-push-3 col-md-6 col-md-push-3">
          <div class="icon scroll-prepare"></div>
          <p class="quote">'.$post->post_content.'</p>
          <center>
          <div class="author clearfix">
          <div class="image" style="background-image:url('.wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0].')"></div>
          <div class="by">
          <p class="name brand-primary-color">'.$meta['testimonial_name'][0].'</p>
          <p class="title">'.$meta['testimonial_title'][0].'</p>
          </div>
          </div>
          </center>
          </div>
          </div>
          </div>';
        }

    }
    wp_reset_query();  // Restore global post data stomped by the_post().

    $html .= '
    </div>

    </div>
    </div>
    </div>
    ';

    echo $html;
  }

  // Widget Backend
  public function form( $instance ) {
    ?>
    <p>
      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('backgroundImage'); ?>"><?php _e('Background image'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('backgroundImage'); ?>" style="width: 60%;" name="<?php echo $this->get_field_name('backgroundImage'); ?>" type="text" value="<?php echo esc_attr($instance['backgroundImage']); ?>" />
      <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
    </p><br />
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['backgroundImage'] 		   = ( ! empty( $new_instance['backgroundImage'] ) ) ? strip_tags( $new_instance['backgroundImage'] ) : '';
    return $instance;
  }
}

// Register and load the widget
function wpb_load_ap_testimonial() {
  register_widget('wpb_ap_testimonial');
}
add_action('widgets_init', 'wpb_load_ap_testimonial');
?>
