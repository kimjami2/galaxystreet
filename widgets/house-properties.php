<?PHP
class wpb_ap_house_properties extends WP_Widget {

   function __construct() {
      parent::__construct(
      // widget ID
      'ap_gallery_properties',

      // widget title
      __('GalaxyStreet House Properties', 'wpb_widget_domain'),

      // widget params
      array(
         'description' => __( 'AP House Properties', 'wpb_widget_domain' ),
         'panels_groups' => array('galaxystreet'),
         'panels_icon' => 'ap-logo-icon'
         )
      );
   }

   // admin widget
   public function widget( $args, $instance ) {

      $title = apply_filters( 'widget_title', $instance['title'] );
      $bgcolor = !empty($instance['bgcolor']) ? 'brand-background' : '';

      /*
      * HTML
      */

      // Javascript for counter
      $html = '
      <script type="text/javascript">
      var sqft = '.$instance["sqft"].';
      var bedrooms = '.$instance["bedrooms"].';
      var bathrooms = '.$instance["bathrooms"].';
      var parkings = '.$instance["parkings"].';
      var buildYear = '.$instance["buildyear"].';
      </script>
      <div id="house-properties" class="container-fluid clearfix gs-section '.$bgcolor.'">
      ';

      // Title & Description
      if(!empty($instance['title']) || !empty($instance['description'])) {
         $html .= '
         <div class="container">
         <div class="row">
         <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-10 col-sm-push-1">';

         $html .= !empty($instance['title']) ? '<h2>'.$instance['title'].'</h2><div class="line"></div>' : false;
         $html .= !empty($instance['description']) ? '<p class="desc">'.$instance['description'].'</p>' : false;

         $html .= '
         </div>
         </div>
         </div>';
      }

      $html .= '<div class="items">';

      // check how many columns we need
      $countProperties = 1;

      if(empty($instance['sqft'])) {
         $countProperties++;
      }
      if(empty($instance['bedrooms'])) {
         $countProperties++;
      }
      if(empty($instance['bathrooms'])) {
         $countProperties++;
      }
      if(empty($instance['parkings'])) {
         $countProperties++;
      }
      if(empty($instance['buildyear'])) {
         $countProperties++;
      }

      if(!empty($instance["sqft"])) {
         $html .= '
         <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
         <div class="house-size icon"></div>
         <p class="desc"><span id="house-sqft">'.$instance["sqft"].'</span> '.$instance["sqftunit"].'</p>
         <p class="title">'.__('Size', 'galaxystreet').'</p>
         </div>';
      }

      if(!empty($instance["bedrooms"])) {
         $html .= '
         <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
         <div class="bedrooms icon"></div>
         <p class="desc"><span id="house-bedrooms">'.$instance["bedrooms"].'</span></p>
         <p class="title">'.__('Bedrooms', 'galaxystreet').'</p>
         </div>';
      }

      if(!empty($instance["bathrooms"])) {
         $html .= '
         <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
         <div class="bathrooms icon"></div>
         <p class="desc"><span id="house-bathrooms">'.$instance["bathrooms"].'</span></p>
         <p class="title">'.__('Bathrooms', 'galaxystreet').'</p>
         </div>';
      }

      if(!empty($instance["parkings"])) {
         $html .= '
         <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
         <div class="garages icon"></div>
         <p class="desc"><span id="house-parkings">'.$instance["parkings"].'</span></p>
         <p class="title">'.__('Parking', 'galaxystreet').'</p>
         </div>';
      }

      if(!empty($instance["buildyear"])) {
         $html .= '
         <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
         <div class="build-year icon"></div>
         <p class="desc"><span id="house-build-year">'.$instance["buildyear"].'</span></p>
         <p class="title">'.__('Build Year', 'galaxystreet').'</p>
         </div>';
      }

      $html .= '
      </div>
      </div>';

      echo $html;
   }

   // Widget Backend
   public function form( $instance ) {
      ?>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title' ); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description' ); ?></label>
         <textarea class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $instance['description'] ); ?></textarea>
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'sqft' ); ?>"><?php _e( 'Sqft' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'sqft' ); ?>" name="<?php echo $this->get_field_name( 'sqft' ); ?>" type="text" value="<?php echo esc_attr( $instance['sqft'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'sqftunit' ); ?>"><?php _e( 'Sqft unit' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'sqftunit' ); ?>" name="<?php echo $this->get_field_name( 'sqftunit' ); ?>" type="text" value="<?php echo esc_attr( $instance['sqftunit'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'bedrooms' ); ?>"><?php _e( 'Bedrooms' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'bedrooms' ); ?>" name="<?php echo $this->get_field_name( 'bedrooms' ); ?>" type="text" value="<?php echo esc_attr( $instance['bedrooms'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'bathrooms' ); ?>"><?php _e( 'Bathrooms' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'bathrooms' ); ?>" name="<?php echo $this->get_field_name( 'bathrooms' ); ?>" type="text" value="<?php echo esc_attr( $instance['bathrooms'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'parkings' ); ?>"><?php _e( 'Parkings' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'parkings' ); ?>" name="<?php echo $this->get_field_name( 'parkings' ); ?>" type="text" value="<?php echo esc_attr( $instance['parkings'] ); ?>" />
      </p>
      <p>
         <label class="ap-widget-input-label" for="<?php echo $this->get_field_id( 'buildyear' ); ?>"><?php _e( 'Buildyear' ); ?></label>
         <input class="" id="<?php echo $this->get_field_id( 'buildyear' ); ?>" name="<?php echo $this->get_field_name( 'buildyear' ); ?>" type="text" value="<?php echo esc_attr( $instance['buildyear'] ); ?>" />
      </p>
      <p class="ap-widget-margin-top">
        <input class="checkbox" type="checkbox" <?php checked( $instance[ 'bgcolor' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" />
        <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>">Theme background color</label>
      </p>
      <?php
   }

   // Updating widget replacing old instances with new
   public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title']			  = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['description'] 	= ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
      $instance['sqft'] 		    = ( ! empty( $new_instance['sqft'] ) ) ? strip_tags( $new_instance['sqft'] ) : '';
      $instance['sqftunit'] 		= ( ! empty( $new_instance['sqftunit'] ) ) ? strip_tags( $new_instance['sqftunit'] ) : '';
      $instance['bedrooms'] 		= ( ! empty( $new_instance['bedrooms'] ) ) ? strip_tags( $new_instance['bedrooms'] ) : '';
      $instance['bathrooms'] 		= ( ! empty( $new_instance['bathrooms'] ) ) ? strip_tags( $new_instance['bathrooms'] ) : '';
      $instance['parkings'] 		= ( ! empty( $new_instance['parkings'] ) ) ? strip_tags( $new_instance['parkings'] ) : '';
      $instance['buildyear'] 		= ( ! empty( $new_instance['buildyear'] ) ) ? strip_tags( $new_instance['buildyear'] ) : '';
      $instance['bgcolor'] 		  = $new_instance['bgcolor'];
      return $instance;
   }
}

// Register and load the widget
function wpb_load_ap_house_properties() {
   register_widget( 'wpb_ap_house_properties' );
}
add_action( 'widgets_init', 'wpb_load_ap_house_properties' );
?>
