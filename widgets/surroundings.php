<?PHP
class wpb_ap_surroundings extends WP_Widget {

  private $APIKey;

  function __construct() {
    parent::__construct(
    // widget ID
    'ap_surroundings',

    // widget title
    __('GalaxyStreet Surroundings', 'wpb_widget_domain'),

    // widget params
    array(
      'description' => __( 'AP Surroundings', 'wpb_widget_domain' ),
      'panels_groups' => array('galaxystreet'),
      'panels_icon' => 'ap-logo-icon'
      )
    );
  }


  // admin widget
  public function widget( $args, $instance ) {

    $bgcolor = !empty($instance['bgcolor']) ? 'brand-background' : '';

    $html .= '
    <div id="surroundings" class="container-fluid gs-section '.$bgcolor.'">
    <div class="container">
    <div class="row title-desc">
    <div class="col-md-8 col-md-push-2">
    <h2>Nearby places</h2><div class="line"></div>
    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
    </div>
    </div>
    <div class="row">
    <div class="map col-md-12">
    <div class="google-map" id="nearby-places-map" style="height: 560px;"></div>
    <div class="places clearfix">

    </div>
    </div>
    </div>
    </div>
    </div>

    <script>
    var surroundingsOptions = {
      "limit": '.($instance['limit'] ? $instance['limit'] : 10).',
      "zoom": '.($instance['zoom'] ? $instance['zoom'] : 17).',
      "radius": '.($instance['radius'] ? $instance['radius'] : 200).',
      "language": "'.($instance['language'] ? $instance['language'] : 'en').'",
      "key": "'.$instance['key'].'"
    };

    jQuery(document).ready(function() {
      GalaxyStreet.surroundings("'.urlencode($instance['address']).'");
    });
    </script>
    ';

    echo $html;
  }

  // Widget Backend
  public function form( $instance ) {
    ?>
    <p>
      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address'); ?></label>
      <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>"><?php echo esc_attr( $instance['address'] ); ?></textarea>

      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Max results'); ?></label>
      <input id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="number" value="<?php echo esc_attr( $instance['limit'] ); ?>" />

      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('zoom'); ?>"><?php _e('Zoom'); ?> <span style="color: #9e9e9e; font-style: italic;">(5-20)</span></label>
      <input id="<?php echo $this->get_field_id( 'zoom' ); ?>" name="<?php echo $this->get_field_name( 'zoom' ); ?>" type="number" value="<?php echo esc_attr( $instance['zoom'] ); ?>" />

      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('radius'); ?>"><?php _e('Radius'); ?> <span style="color: #9e9e9e; font-style: italic;">(200-1000)</span></label>
      <input id="<?php echo $this->get_field_id( 'radius' ); ?>" name="<?php echo $this->get_field_name( 'radius' ); ?>" type="number" value="<?php echo esc_attr( $instance['radius'] ); ?>" />

      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('radius'); ?>"><?php _e('Language code'); ?> <span style="color: #9e9e9e; font-style: italic;">(default: en)</span></label>
      <input id="<?php echo $this->get_field_id( 'language' ); ?>" name="<?php echo $this->get_field_name( 'language' ); ?>" type="text" value="<?php echo esc_attr( $instance['language'] ); ?>" />
      <p>List of all supported <a href="https://developers.google.com/maps/faq#languagesupport">languages</a></p>

      <label class="ap-widget-input-label" for="<?php echo $this->get_field_id('key'); ?>"><?php _e('Google API Key'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'key' ); ?>" name="<?php echo $this->get_field_name( 'key' ); ?>" type="text" value="<?php echo esc_attr( $instance['key'] ); ?>" />
      <p>Don't have an API key? Go to <a target="_new" href="https://developers.google.com/places/web-service/get-api-key">developers.google.com</a> and generate one.</p>
    </p>
    <p class="ap-widget-margin-top">
      <input class="checkbox" type="checkbox" <?php checked( $instance[ 'bgcolor' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" />
      <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>">Theme background color</label>
    </p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['address']    = (!empty( $new_instance['address'])) ? $new_instance['address'] : '';
    $instance['limit']      = (!empty( $new_instance['limit'])) ? $new_instance['limit'] : '';
    $instance['zoom']       = (!empty( $new_instance['zoom'])) ? $new_instance['zoom'] : '';
    $instance['radius']     = (!empty( $new_instance['radius'])) ? $new_instance['radius'] : '';
    $instance['language']   = (!empty( $new_instance['language'])) ? $new_instance['language'] : '';
    $instance['key']        = (!empty( $new_instance['key'])) ? $new_instance['key'] : '';
    $instance['bgcolor'] 		= $new_instance['bgcolor'];

    return $instance;
  }
}

// Register and load the widget
function wpb_load_ap_surroundings() {
  register_widget('wpb_ap_surroundings');
}
add_action('widgets_init', 'wpb_load_ap_surroundings');
?>
