/* ========================================================================
* DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
*
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.
*
* .noConflict()
* The routing is enclosed within an anonymous function so that you can
* always reference jQuery with $, even when in .noConflict() mode.
*
* Google CDN, Latest jQuery
* To use the default WordPress version of jQuery, go to lib/config.php and
* remove or comment out: add_theme_support('jquery-cdn');
* ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Roots = {
    // All pages
    'common': {
      init: function() {

        // mobile menu
        jQuery('.mobile-menu-nav .menu').on('click', function() {
          var elements = jQuery('.mobile-menu, .mobile-menu-nav .menu');
          if(elements.hasClass('open')) {
            elements.removeClass('open');
          } else {
            elements.addClass('open');
          }
        });

        jQuery('.mobile-menu ul li .dropdown-toggle').on('click', function() {
          var dropdown = jQuery(this).next('.dropdown-menu'),
              back = jQuery('.mobile-menu-nav .back button'),
              menu = jQuery('.mobile-menu ul.menu');

          menu.addClass('gone');
          dropdown.addClass('show');
          back.addClass('show');
        });

        jQuery('.mobile-menu-nav .back button').on('click', function() {
          var dropdown = jQuery('.mobile-menu ul .dropdown-menu.show'),
              back = jQuery('.mobile-menu-nav .back button'),
              menu = jQuery('.mobile-menu ul.menu');

            menu.removeClass('gone');
            dropdown.removeClass('show');
            back.removeClass('show');
        });



        // set page-section offset
        //jQuery('.page-section').css('margin-top', (jQuery('#header').height() - 3)+'px');

        // send contact email
        $('.contact-form .send-contact-email').on('click', function() {

          if (!$('.contact-form')[0].checkValidity()) {
            $('.contact-form').find('input[type="submit"]').click();
            return false;
          }

          if(!$(this).hasClass('btn-loading')) {

            //button loading
            var oldLabel = $(this).html();
            var btn = $(this);
            $(this).addClass('btn-loading');
            $(this).html('<i class="fa fa-spinner spin"></i>');

            $.ajax({
              url: '/wp-admin/admin-ajax.php',
              data: {
                'action':'send_contact_email',
                'first-name': $('#first-name').val(),
                'last-name': $('#last-name').val(),
                'email': $('#email').val(),
                'message': $('#message').val()
              },
              method: 'POST',
              success:function(data) {

                // show modal
                $('.mail-sent-conf').slideToggle('fast').delay(3000).slideToggle('fast');
                // reverse button
                $(btn).removeClass('btn-loading');
                $(btn).html(oldLabel);
              },
              error: function(errorThrown){
                console.log('send_contact_email_error', errorThrown);
              }
            });
          }

        });

        // gallery filtering
        $('.gallery-filtering').on('click', function(e) {
          var categorySlug = $(this).data('category-slug');
          activeGalleryFilteringButton(categorySlug);
          filterGallery(categorySlug);
        });

        function activeGalleryFilteringButton(categorySlug) {
          $('.gallery-filtering').removeClass('active');
          $('.gallery-filtering-'+categorySlug).addClass('active');
        }

        function filterGallery(categorySlug) {
          if(categorySlug === 'all-images') {
            $('.gallery-row').slideDown('fast');
          } else {
            $('.gallery-row').each(function(i, row) {
              if(!$(row).hasClass('category-'+categorySlug)) {
                $(row).hide();
              } else {
                $(row).show();
              }
            });
          }
        }

        setTimeout(function() {
          $('.loading').fadeOut(500);
        }, 0);

        // Testimonials slider
        $('.testimonials-slide').simpleImageSlider({fullscreen: false, overlay: false, autoRotate: true, autoRotateTime: 5000});


        // home about section
        function setHomeAboutHeight() {
          if($('.home-about').length) {

            $('.home-about').each(function(index, elm) {
              var image = $(elm).find('.image');
              var info = $(elm).find('.info');

              if(info.height() >= image.height()) {
                image.height(info.height());
              }
            });
          }
        }

        setHomeAboutHeight();

        function setSmallMenuDimensions() {
          var menu = jQuery('.mobile-menu'),
              windowElm = jQuery(window);
          menu.height(windowElm.height()).width(windowElm.width());
        }

        setSmallMenuDimensions();

        $(window).on('resize', function() {
          setHomeAboutHeight();
          setSmallMenuDimensions();
        });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {

        // resize menu
        $(document).scroll(function() {
          if(!whiteHeader) {
            if($(document).scrollTop() > 0) {
              $('#header').addClass('slim');
            } else {
              $('#header').removeClass('slim');
            }
          } else {
            if($(document).scrollTop() > 20) {
              $('#header').addClass('hide-head');
            } else {
              $('#header').removeClass('hide-head');
            }
          }
        });

        // always show white header
        if(whiteHeader) {
          $('#header').addClass('slim').addClass('show-top-start');
        }

        // resize menu if user is below the scroll top threshold
        if($(document).scrollTop() > 0) {
          $('#header').addClass('slim');
        }

        // scroll fire
        var options = [
          {selector: '#home-gallery-divider', offset: 30, callback: '$(\'#home-gallery-divider\').addClass(\'scroll-animate\');' },
          {selector: '#home-about-divider', offset: 30, callback: '$(\'#home-about-divider\').addClass(\'scroll-animate\');' },
          {selector: '#home-agent-divider', offset: 30, callback: '$(\'#home-agent-divider\').addClass(\'scroll-animate\');' },
          {selector: '#house-properties', offset: 150, callback: 'countHouseProperties();' },
          //{selector: '.home-testimonials-info', offset: 300, callback: '$(\'.home-testimonials-info\').addClass(\'scroll-animate\');' },
          {selector: '#home-gallery-btn', offset: 20, callback: '$(\'#home-gallery-btn\').addClass(\'scroll-animate\');' },
          {selector: '#home-about-btn', offset: 20, callback: '$(\'#home-about-btn\').addClass(\'scroll-animate\');' },
        ];

        Materialize.scrollFire(options);

        var resizeBanner = function() {
          var height = ( $(window).height() > 400) ? $(window).height() : 400;
          $('#banner').height(height);
          $('.hero-gallery').height(height);
          $('.rsContent').height(height);

        };


        $(window).on('resize', function() {
          resizeBanner();
        });

        // resize banner
        if($('#banner').length > 0) {
          resizeBanner();
        }

        // Banner slider
        $('#banner .image-slider').simpleImageSlider({fullscreen: true, overlay: true, autoRotate: false, autoRotateTime: 5000});


        // animate init
        setTimeout(function() {
          $('#banner .container-fluid').addClass('dark');
          $('#banner-info').addClass('scroll-animate');
        }, 600);

        setTimeout(function() {
          $('#header').addClass('scroll-animate');
          $('.contact-box').addClass('scroll-animate');
        }, 1000);

      },
      finalize: function() {
      },
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };


  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var namespace = Roots;
      funcname = (funcname === undefined) ? 'init' : funcname;
      if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      UTIL.fire('common');

      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        // console.log(classnm);
        UTIL.fire(classnm);
      });
    }
  };

  $(document).ready(UTIL.loadEvents);

})(jQuery);


var GalaxyStreet = {

  surroundings: function(address) {
    GalaxyStreet.getLatLng(address).done(GalaxyStreet.getNearbyPlaces);
  },

  /*
  * Get latitude nad longitude from googleapis.com
  */
  getLatLng: function(address) {
    return $.ajax({
      url: 'http://maps.googleapis.com/maps/api/geocode/json?address='+address,
      type: 'GET',
    });
  },

  /*
  * Get nearby places from googleapis.com
  */
  getNearbyPlaces: function(latLng) {

    var data = {
      'action': 'nearby_places',
      'latLng': latLng,
      'options': surroundingsOptions
    };

    return $.ajax({
      url: ajaxurl,
      type: 'POST',
      dataType: "json",
      data: data,
      success: function(data) {
        if(data.status !== 'OK') {
          console.log('Google API error:', data.error_message);
        } else {
          GalaxyStreet.buildSurroundings(data, latLng);
        }
     },
      error: function(data) {
        console.log(data);
      }
    });
  },

  /*
  * Init google maps and HTML markup for surroundings
  */
  buildSurroundings: function(data, latLng) {

    var markers = data.results,
    lat = latLng.results[0].geometry.location.lat,
    lng = latLng.results[0].geometry.location.lng;

    var mapOptions = {
      zoom: surroundingsOptions.zoom,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng(lat, lng),
      styles: [{elementType: 'labels', stylers: [{ visibility: 'off' }]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"color":"#e4eed0"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#639c35"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#fffad8"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f8f79a"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#2b5d18"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#fcfcfc"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#f7f7f4"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#e0e0e0"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#646464"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#f6e881"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#bbdcf1"}]}]
    };

    var map = new google.maps.Map(document.getElementById('nearby-places-map'), mapOptions);

    // create house marker
    new Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
    });

    // create markers
    if(typeof markers !== 'undefined') {
      jQuery.each(markers, function(index, marker) {

        var markerIcon;

        if(typeof marker.type !== 'undefined') {
          markerIcon = marker.type.replace(/_/g, "-");
        } else {
          markerIcon = marker.types[0].replace(/_/g, "-");
        }

        var place = new Marker({
          position: new google.maps.LatLng(marker.geometry.location.lat, marker.geometry.location.lng),
          map: map,
          icon: {
            path: SQUARE_PIN,
            fillColor: marker.color,
            fillOpacity: 1,
            strokeColor: '#FFFFFF',
            strokeWeight: 1
          },
          map_icon_label: '<span class="map-icon map-icon-'+markerIcon+'"></span>'
        });

        /*
        var infoWindow = new google.maps.InfoWindow({
          content: 's',
        });

        google.maps.event.addListener(place, 'mouseover', function () {
          infoWindow.open(map, place);
        });

        google.maps.event.addListener(place, 'mouseout', function () {
          infoWindow.close(map, place);
        });
        */

        // create HTML Table

        var distanceInKm = GalaxyStreet.getDistanceFromLatLonInKm(lat, lng,marker.geometry.location.lat, marker.geometry.location.lng);
        var listPlace = $('<div class="item"><div class="icon"></div><span class="icon map-icon map-icon-'+markerIcon+'" style="color: '+marker.color+';"></span><div class="info"><p>'+marker.name+'</p><p class="type">'+GalaxyStreet.prettyName(marker.type)+'<span class="distance">'+distanceInKm+'km</span></p></div></div>');
        $('.nearby-places .places').append(listPlace).show();

        // resize parallax fallback
        $(window).trigger("resize").trigger("scroll");
      });
    }
  },

  getDistanceFromLatLonInKm: function(lat1,lon1,lat2,lon2) {
    var R = 6371;
    var dLat = GalaxyStreet.deg2rad(lat2-lat1);
    var dLon = GalaxyStreet.deg2rad(lon2-lon1);
    var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(GalaxyStreet.deg2rad(lat1)) * Math.cos(GalaxyStreet.deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    d = Math.round(d*100) / 10;
    return d;
  },

  deg2rad: function(deg) {
    return deg * (Math.PI/180);
  },

  prettyName: function(name) {
    return name.replace('_', ' ');
  },

  maxHeight: function(element) {
    var elm = jQuery('#'+element),
    parent = elm.parent().css('min-height', '100%'),
    parentHeight = elm.parent().height();

    if(parentHeight === 0) {
      parentHeight = elm.parent().parent().height();
    }

    elm.height(parentHeight);
  }
};


// outside functionallity

function countHouseProperties() {
  var options = {
    useEasing : true,
    useGrouping : true,
    separator : ',',
    decimal : '.',
    prefix : '',
    suffix : ''
  };
  var options2 = {
    useEasing : true,
    useGrouping : true,
    separator : '',
    decimal : '.',
    prefix : '',
    suffix : ''
  };

  if(!$('#house-properties').hasClass('is-done')) {
    if($('#house-sqft').length > 0) {
      var houseSqft = new CountUp(document.getElementById("house-sqft"), 0, sqft, 0, 1.5, options);
      houseSqft.start();
    }
    if($('#house-bedrooms').length > 0) {
      var houseBedrooms = new CountUp(document.getElementById("house-bedrooms"), 0, bedrooms, 0, 1.5, options);
      houseBedrooms.start();
    }
    if($('#house-bathrooms').length > 0) {
      var houseBathrooms = new CountUp(document.getElementById("house-bathrooms"), 0, bathrooms, 0, 1.5, options);
      houseBathrooms.start();
    }
    if($('#house-parkings').length > 0) {
      var houseParkings = new CountUp(document.getElementById("house-parkings"), 0, parkings, 0, 1.5, options);
      houseParkings.start();
    }
    if($('#house-build-year').length > 0) {
      var houseBuildYear = new CountUp(document.getElementById("house-build-year"), 0, buildYear, 0, 1.5, options2);
      houseBuildYear.start();
    }

    // store a class to avoid recounts
    $('#house-properties').addClass('is-done');
  }

}

// image lightbox function

$( function()
{
  // ACTIVITY INDICATOR

  var activityIndicatorOn = function()
  {
    $( '<div id="imagelightbox-loading"><div></div></div>' ).appendTo( 'body' );
  },
  activityIndicatorOff = function()
  {
    $( '#imagelightbox-loading' ).remove();
  },

  // OVERLAY

  overlayOn = function()
  {
    $( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
  },
  overlayOff = function()
  {
    $( '#imagelightbox-overlay' ).remove();
  },


  // CLOSE BUTTON

  closeButtonOn = function( instance )
  {
    $( '<button type="button" id="imagelightbox-close" title="Close"></button>' ).appendTo( 'body' ).on( 'click touchend', function(){ $( this ).remove(); instance.quitImageLightbox(); return false; });
  },
  closeButtonOff = function()
  {
    $( '#imagelightbox-close' ).remove();
  },


  // CAPTION

  captionOn = function()
  {
    var description = $( 'a[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ).attr( 'alt' );
    if( description && description.length > 0 ) {
      $( '<div id="imagelightbox-caption">' + description + '</div>' ).appendTo( 'body' );
    }
  },
  captionOff = function()
  {
    $( '#imagelightbox-caption' ).remove();
  },


  // NAVIGATION

  navigationOn = function( instance, selector )
  {
    var images = $( selector );
    if( images.length )
    {
      var nav = $( '<div id="imagelightbox-nav"></div>' );
      for( var i = 0; i < images.length; i++ )
      {
        nav.append( '<button type="button"></button>' );
      }
      nav.appendTo( 'body' );
      nav.on( 'click touchend', function(){ return false; });

      var navItems = nav.find( 'button' );
      navItems.on( 'click touchend', function()
      {
        var $this = $( this );
        if( images.eq( $this.index() ).attr( 'href' ) !== $( '#imagelightbox' ).attr( 'src' ) )
        {
          instance.switchImageLightbox( $this.index() );
        }
        navItems.removeClass( 'active' );
        navItems.eq( $this.index() ).addClass( 'active' );

        return false;
      })
      .on( 'touchend', function(){ return false; });
    }
  },
  navigationUpdate = function( selector )
  {
    var items = $( '#imagelightbox-nav button' );
    items.removeClass( 'active' );
    items.eq( $( selector ).filter( '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ).index( selector ) ).addClass( 'active' );
  },
  navigationOff = function()
  {
    $( '#imagelightbox-nav' ).remove();
  },


  // ARROWS

  arrowsOn = function( instance, selector )
  {
    var $arrows = $( '<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>' );

    $arrows.appendTo( 'body' );

    $arrows.on( 'click touchend', function( e )
    {
      e.preventDefault();

      var $this	= $( this ),
      $target	= $( selector + '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ),
      index	= $target.index( selector );

      if( $this.hasClass( 'imagelightbox-arrow-left' ) )
      {
        index = index - 1;
        if( !$( selector ).eq( index ).length )
        {
          index = $( selector ).length;
        }
      }
      else
      {
        index = index + 1;
        if( !$( selector ).eq( index ).length )
        {
          index = 0;
        }
      }

      instance.switchImageLightbox( index );
      return false;
    });
  },
  arrowsOff = function()
  {
    $( '.imagelightbox-arrow' ).remove();
  };

  var selectorF = 'a[data-imagelightbox="f"]';
  var instanceF = $( selectorF ).imageLightbox(
    {
      onStart:		function() { overlayOn(); closeButtonOn( instanceF ); arrowsOn( instanceF, selectorF ); },
      onEnd:			function() { overlayOff(); captionOff(); closeButtonOff(); arrowsOff(); activityIndicatorOff(); },
      onLoadStart: 	function() { captionOff(); activityIndicatorOn(); },
      onLoadEnd:	 	function() { captionOn(); activityIndicatorOff(); $( '.imagelightbox-arrow' ).css( 'display', 'block' ); }
    });
  });
