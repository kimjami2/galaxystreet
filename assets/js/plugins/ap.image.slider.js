/*
* Simple Image Slider 0.1
* https://github.com/hawkmeister/simple-image-slider
*
* Copyright 2015, Kim Jami
* Free to use and abuse under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
*/

;(function($, window, document, undefined) {

	// predfine default variables
	var $pluginName = "simpleImageSlider";

	$defaults = {
		fullscreen: false,
		overlay: false,
		autoRotate: true,
		autotRotateTime: 4000
	};

	var Plugin = function($element, $options) {

		// predfine our variables
		this.$name = $pluginName;
		this.$settings = $.extend({}, $defaults, $options);
		this.$slider = $($element);
		this.$slides = this.$slider.children(),
		this.$countSlides = this.$slides.length;
		this.$currentSlideIndex = 0;

		// call the constructor
		this.init();
	}

	Plugin.prototype = {
		init: function() {

			// set image slider class
			this.$slider.addClass('simple-image-slider');

			// resize slider if fullscreen mode
			this.resizeSlider();

			// bind watch for slider resize
			this.watchSliderResize(this);

			// build our slides
			this.remodelSlides(this);

			// update slide dimensions
			this.updateSlideDimensions();

			// update slide offsets
			this.updateSlidesOffset();

			// apply controllers
			this.applyController(this);

			// show the slider
			this.$slider.show();

			// auto rotate
			this.autoRotate(this);

		},

		/*
		* Check if slider should be fullscreen, if so resize accordingly to $(window) dimensions
		*/

		resizeSlider: function() {
			if(this.$settings.fullscreen) {
				this.$slider.height(window.innerHeight);
			}
		},

		/*
		* Watch for window resize and apply it to the slider
		*/

		watchSliderResize: function(self) {
			$(window).on('resize', function() {

				// do not animate resizing
				self.$slides.removeClass('animate');

				// check if we should resize the slider
				if (self.$settings.fullscreen) {
					self.resizeSlider();
				}

				self.updateSlideDimensions();
				self.updateSlidesOffset();

			});
		},

		/*
		* Parse and create our slides and apply it to the DOM
		*/

		remodelSlides: function(self) {
			this.$slides.each(function(index, value) {

				// set our variables
				var $slide = $(value),
				$image = $slide.find('img'),
				$imageSrc = $image.attr('src'),
				$imageCaption = $slide.find('p');

				// remove image and caption element
				$image.remove();

				// create a div with the imageSrc as background
				if(typeof $imageSrc !== 'undefined') {
					$image = $('<div class="image"></div>')
					$image.css('background-image', 'url(' + $imageSrc + ')');
					$slide.prepend($image);
				}

				// append overlay if settings allows
				if (self.$settings.overlay) {
					$overlay = $('<div class="overlay"></div>');
					$image.after($overlay);
				}

				// remodel our caption
				if ($imageCaption != 'undefined') {
					$imageCaption.addClass('caption');
				}

				// show the slide hidden by css and apply the animation class
				$slide.show();

				// add current class to our first slide
				if (index == 0) {
					$slide.addClass('current');
				}

			});
		},

		/*
		* Update our slides dimensions accordingly
		*/

		updateSlideDimensions: function() {
			this.$slides.css({
				height: this.$slider.height(),
				width: this.$slider.width()
			});
		},

		/*
		* Set slides offset based on slider width
		*/

		updateSlidesOffset: function() {
			// get slide dimensions
			var $slideDimensions = {
				height: this.$slider.height(),
				width: this.$slider.width()
			},
			$currentSlideIndex = this.$currentSlideIndex;

			this.$slides.each(function(index, value) {
				var $slide = $(value);
				$slide.css('margin-left', ($slideDimensions.width * index) - ($slideDimensions.width * $currentSlideIndex) + 'px');
			});
		},

		/*
		* Create our controller view and bind events
		*/

		applyController: function(self) {

			// check if controller is needed
			if(this.$slider.find('.slide').length < 2) {
				return;
			}

			// create our elements
			var $ctrlContainer = $('<div class="controller"></div>'),
			$ctrlNavLeft = $('<div class="nav-left"></div>'),
			$ctrlNavRight = $('<div class="nav-right"></div>');

			// assemble our controller
			$controller = $ctrlContainer.append($ctrlNavLeft).append($ctrlNavRight);
			this.$slider.append($controller);

			// bind navigation actions
			$ctrlNavRight.on('click', function(e) {
				e.preventDefault();
				self.slideNext(self);
			});

			$ctrlNavLeft.on('click', function(e) {
				e.preventDefault();
				self.slidePrev(self);
			});
		},

		/*
		* Slide next
		*/

		slideNext: function(self) {
			self.$currentSlideIndex++;

			// animate slides
			this.$slides.addClass('animate');

			var $currentSlide = this.$slider.find('.current'),
			$nextSlide = $currentSlide.next('.slide');

			// remove current from prev slide and add to the next slide
			$currentSlide.removeClass('current');
			$nextSlide.addClass('current');

			// if it's the last slider we go to the first
			if (self.$currentSlideIndex === self.$countSlides) {
				self.resetSlider(self);
				return;
			}

			// get slide dimensions
			var $slideDimensions = {
				height: self.$slider.height(),
				width: self.$slider.width()
			};

			// decrease margin on slides
			self.$slides.each(function(index, slide) {
				$(slide).css('margin-left', ($slideDimensions.width * index) - ($slideDimensions.width * self.$currentSlideIndex) + 'px');
			});

			// cancel autorotate
			self.autoRotate(self);
		},

		/*
		* Slide prev
		*/

		slidePrev: function(self) {
			self.$currentSlideIndex--;

			// animate slides
			this.$slides.addClass('animate');

			var $currentSlide = this.$slider.find('.current'),
			$prevSlide = $currentSlide.prev('.slide');

			// remove current from prev slide and add to the next slide
			$currentSlide.removeClass('current');
			$prevSlide.addClass('current');

			// if it's the first slide we go to the last
			if (self.$currentSlideIndex < 0) {
				self.$currentSlideIndex = self.$countSlides - 1;
			}

			// get slide dimensions
			$slideDimensions = {
				height: self.$slider.height(),
				width: self.$slider.width()
			};

			// decrease margin on slides
			self.$slides.each(function(index, slide) {
				$(slide).css('margin-left', ($slideDimensions.width * index) - ($slideDimensions.width * self.$currentSlideIndex) + 'px');
			});

			// cancel autorotate
			self.autoRotate(self);
		},

		/*
		* Reset slider to its orginal state
		*/

		resetSlider: function(self) {
			this.$currentSlideIndex = 0;
			this.updateSlidesOffset();
			this.$slider.find('.slide:first-child').addClass('current');

			// reactive auto rotator
			this.autoRotate(self);
		},

		/*
		* Auto rotate images with timer
		*/

		autoRotate: function(self) {
			if(self.$settings.autoRotate && self.$settings.autoRotateTime > 0) {
				clearTimeout(this.$timer);
				this.$timer = setTimeout(function() { self.slideNext(self); }, this.$settings.autoRotateTime)
			}
		}

	}

	// init plugin and avoid double initilization
	$.fn[$pluginName] = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + $pluginName)) {
				$.data(this, "plugin_" + $pluginName,
				new Plugin(this, options));
			}
		});
	};

})(jQuery, window, document);
