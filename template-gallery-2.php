<?php
/*
Template Name: Gallery 2 column
*/
?>

<?php
$taxonomies = array(
  'gallery_categories'
);

$args = array(
  'orderby'           => 'name',
  'order'             => 'ASC',
  'hide_empty'        => true,
);
$terms = get_terms($taxonomies, $args);
?>

<?php while (have_posts()) : the_post(); ?>

  <div id="gallery" class="container-fluid 1-column">
    <?php
    if(atomic_option('atomic_gallery_header') == 'true') {
      ?>
      <div class="row head parallax-window" data-parallax="scroll" data-image-src="<?PHP echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large') ?>">
        <div class="overlay">
          <div class="text-wrapper">
            <div class="text">
              <h2><?PHP echo the_title(); ?></h2>
              <p class="desc"><?PHP echo wp_trim_words(the_content(), 200, '..'); ?></p>
            </div>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
    <div class="row categories">
      <div class="container">
        <a href="javascript:void(0);" class="item gallery-filtering active gallery-filtering-all-images" data-category-slug="all-images">
          <?PHP _e('All', 'galaxystreet'); ?>
        </a>
        <?php
        foreach($terms as $key => $category) {
          ?>
          <a href="javascript:void(0);" class="item gallery-filtering gallery-filtering-<?php echo $category->slug; ?>" data-category-slug="<?php echo $category->slug; ?>">
            <?php echo $category->name; ?>
          </a>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="container images">
      <?php
      foreach($terms as $key => $category) {

        $args = array(
          'post_type' => 'gallery_images',
          'gallery_categories' => $category->slug
        );
        $query = new WP_Query( $args );
        ?>
        <div class="clearfix gallery-row category-<?php echo $category->slug; ?>">
          <div class="category-title">
            <h3><?php echo $category->slug; ?></h3>
          </div>
          <div class="row">
            <?php
            while ($query->have_posts()) {
              $query->the_post();
              ?>
              <div class="item col-lg-6 col-md-6 col-sm-12">
                <a href="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0]; ?>" alt="<?php echo the_content(); ?>" class="overlay" data-imagelightbox="f">
                  <div class="icon">
                    <div class="zoom">
                      <i class="mdi mdi-plus-circle"></i>
                    </div>
                  </div>
                  <?php
                  if($post->post_content!="") {
                    ?>
                    <div class="desc">
                      <?php echo the_content(); ?>
                    </div>
                    <?php
                  }
                  ?>
                </a>
                <div class="img" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0]; ?>);"></div>
              </div>
              <?php
            }
            $count = 0;
            wp_reset_postdata();
            ?>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>

<?php endwhile; ?>


<?php
// select a category based on the URL input

if(isset($_GET['category']) && $_GET['category'] != '') {
  echo "
  <script type='text/javascript'>
  jQuery(document).ready(function() {
    var categorySlug = '".$_GET['category']."';
    $('.gallery-filtering').removeClass('active');
    $('.gallery-filtering-'+categorySlug).addClass('active');

    $('.gallery-row').each(function(i, row) {
      if(!$(row).hasClass('category-'+categorySlug)) {
        $(row).hide('fast');
      }
    });
  });
  </script>
  ";
}

?>
