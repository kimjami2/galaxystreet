<?php get_template_part('templates/head'); ?>
<body <?php body_class(''); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header');
  ?>

  <?php if (
  is_page_template('template-home.php') ||
  is_page_template('template-gallery-1.php') ||
  is_page_template('template-gallery-2.php') ||
  is_page_template('template-gallery-3.php') ||
  is_page_template('template-gallery-1-full.php') ||
  is_page_template('template-gallery-2-full.php') ||
  is_page_template('template-gallery-3-full.php') ||
  is_page_template('index.php') ||
  is_page_template('template-contact.php')

  ) : ?>

  <div class="wrap" role="document">
    <div class="content clearfix">
      <main class="main" role="main">
        <?php include roots_template_path(); ?>
      </main>
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php else: ?>

  <?php get_template_part('templates/page', 'header'); ?>

  <div class="wrap container" role="document">
    <div class="content row">
      <main class="main" role="main">
        <?php include roots_template_path(); ?>
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <!--
            <aside class="sidebar" role="complementary">
              <?php //include roots_sidebar_path(); ?>
            </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php endif; ?>

  <?php get_template_part('templates/footer'); ?>

  <?php wp_footer(); ?>

</body>
</html>
