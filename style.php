<?php
$apTitleFont = atomic_option_font(atomic_option('atomic_typography_title_font'));
$apTextFont = atomic_option_font(atomic_option('atomic_typography_content_font'));

$image_logo_width_small 	= atomic_option('atomic_logo_image_width') * 0.80;
$image_logo_height_small 	= atomic_option('atomic_logo_image_height') * 0.80;
?>

/*
* typography
*/


@import url('https://fonts.googleapis.com/css?family=<?php echo $apTitleFont['name']; ?>:200,300,400,500,600,700,800');
@import url('https://fonts.googleapis.com/css?family=<?php echo $apTextFont['name']; ?>:200,300,400,500,600,700,800');

body {
  font-family: '<?php echo $apTextFont['title']; ?>', sans-serif;
}

h1, h2, h3, h4, h5 {
  font-family: '<?php echo $apTitleFont['title']; ?>', sans-serif;
}

h1 {
  font-size: <?php atomic_option('atomic_typography_h1', true); ?>px;
}

h2 {
  font-size: <?php atomic_option('atomic_typography_h2', true); ?>px;
}

h3 {
  font-size: <?php atomic_option('atomic_typography_h3', true); ?>px;
}

<?php if(atomic_option('atomic_logo_image_shrink') == 'true') { ?>
#header.slim .logo {
  width: <?php echo $image_logo_width_small; ?>px !important;
  height: <?php echo $image_logo_height_small; ?>px !important;
}
<?php } ?>

<?php if(atomic_option('atomic_header_top_info') == 'false' && atomic_option('atomic_header_top_social') == 'false') { ?>
#header .top-info {
  display: none;
}
#header .seperator {
  display: none;
}
#header.slim {
  transform: translate(0,0);
}
<?php } ?>

<?php if(atomic_option('atomic_header_top_info_mobile') == 'false') { ?>
#header .mobile-menu .info {
display: none;
}
<?php } ?>

<?php if(atomic_option('atomic_header_top_social_mobile') == 'false') { ?>
#header .mobile-menu .social {
display: none;
}
<?php } ?>

<?php if(!empty(atomic_option('atomic_logo_size'))) { ?>
#header .menu .logo a {
font-size: <?php atomic_option('atomic_logo_size', true); ?>px;
}
<?php } ?>
<?php if(!empty(atomic_option('atomic_logo_size_small'))) { ?>
@media screen and (max-width: 640px) {
  #header .menu .logo a {
    font-size: <?php atomic_option('atomic_logo_size_small', true); ?>px !important;
  }
}
<?php } ?>
<?php if(!empty(atomic_option('atomic_gallery_corners'))) { ?>
#gallery .images .img {
    border-radius: <?php atomic_option('atomic_gallery_corners', true); ?>px !important;
}
<?php } ?>
<?php if(atomic_option('atomic_gallery_header') == 'false') { ?>
#gallery .head {
  display: none;
}
<?php } ?>

<?php if(atomic_option('atomic_header_top_scroll', false) == 'false') { ?>
  #header.fixed:hover .head {
    margin-top: -46px !important;
  }
<?php } ?>

<?php if(atomic_option('atomic_header_top_center', false) == 'true') { ?>

  .image-slider .container {
    position: absolute !important;
    top: 0;
    right: 0;
    left: 0;
    height: 100%;
    width: 100%;
    padding-top: 0px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

    .image-slider .container .contact-now {
      margin-top: 20px;
      border-top: 1px solid rgba(255,255,255,0.4);
    }

      .image-slider .container .contact-now .wrapper {
        position: static !important;
        width: 100% !important;
        text-align: center;
      }

    .image-slider .container .caption-wrapper {
      position: static;
      left: calc(50% - 300px);
      margin-left: auto;
      margin-right: auto;
      text-align: center;
    }

    @media (max-width: 768px) {
      .image-slider .slide .caption-wrapper {
        padding-left: 50px;
        padding-right: 50px;
      }
    }
<?php } ?>

<?php if(atomic_option('atomic_header_top_white', false) == 'true') { ?>
#header .head {
  position: absolute;
  top: 138px;
  width: 100%;
  background: transparent !important;
  color: #FFF;
}

  #header .head .container .info p {
    color: #FFF;
  }
  #header .head .container .social a {
    color: #FFF;
  }

  #header.fixed:hover .head {
    margin-top: -46px;
  }

  #header.fixed .menu {
    padding-top: 10px;
  }

  <?php if(!is_front_page()) { ?>
  #header .head {
    display: none !important;
  }
  <?php } ?>
<?php } ?>




@media (max-width: 1200px) {

  #header .logo {
    width: <?php echo $image_logo_width_small; ?>px !important;
    height: <?php echo $image_logo_height_small; ?>px !important;
  }

}
