<?php
/**
* Roots includes
*
* The $roots_includes array determines the code library included in your theme.
* Add or remove files to the array as needed. Supports child theme overrides.
*
* Please note that missing files will produce a fatal error.
*
* @link https://github.com/roots/roots/pull/1042
*/
$roots_includes = array(
   'lib/utils.php',           // Utility functions
   'lib/init.php',            // Initial theme setup and constants
   'lib/wrapper.php',         // Theme wrapper class
   'lib/sidebar.php',         // Sidebar class
   'lib/config.php',          // Configuration
   'lib/activation.php',      // Theme activation
   'lib/titles.php',          // Page titles
   'lib/nav.php',             // Custom nav modifications
   'lib/scripts.php',         // Scripts and stylesheets
   'lib/extras.php',          // Custom functions
   'lib/page-builder.php',
   'lib/ajax-calls.php'
);

foreach ($roots_includes as $file) {
   if (!$filepath = locate_template($file)) {
      trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
   }

   require_once $filepath;
}
unset($file, $filepath);

/*---------------------------------------------------
-----------------------------------------------------
Atomic Pixel
-----------------------------------------------------
----------------------------------------------------*/

/*
* Remove empty fix <p></p>
*/

remove_filter('the_content', 'wpautop');

/*---------------------------------------------------
Atomic Custom Post Types
----------------------------------------------------*/
include_once('lib/custom_posts.php');

/*---------------------------------------------------
Atomic Shortcodes
----------------------------------------------------*/
// include plugin.php to use is_plugin_active function
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

include_once('lib/shortcodes.php');

/*---------------------------------------------------
Atomic Pixel Visual Composer & Widgets
----------------------------------------------------*/

foreach ( glob( plugin_dir_path( __FILE__ ) . "vc_templates/*.php" ) as $file ) {
    include_once $file;
}
foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
    include_once $file;
}

add_action('admin_head', 'vc_custom_css');

function vc_custom_css() {
  echo '<style>
    .vc_widget_icon {
      background-image: url(/wp-content/themes/galaxystreet/assets/img/galaxystreet_logo_64x64.png) !important;
      background-size: 32px 32px !important;
      background-position: center center !important;
    }
  </style>';
}

/*
* Atomic Home Page
*/
/*
register_sidebar( array(
	'id'          => 'atomic-home-page',
	'name'        => 'Home page',
	'description' => __( 'Display widgets on your homepage', 'text_domain' ),
) );
*/

/*---------------------------------------------------
Contact form
----------------------------------------------------*/
function send_contact_email(){
   global $wpdb;

   $to = get_option('admin_email');
   $subject = 'New email from '.get_option('blogname');
   $message = '<b>From:</b> '.$_POST['first-name'].' '.$_POST['last-name'].'<br /><b>Email:</b> '.$_POST['email'].' <br /><b>Message</b><br />'.$_POST['message'];
   $headers = "MIME-Version: 1.0\r\n";
   $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
   wp_mail($to, $subject, $message, $headers, false);
}
add_action('wp_ajax_send_contact_email', 'send_contact_email');

/*---------------------------------------------------
Atomic Builder Plugin
----------------------------------------------------*/
function atomic_option($option, $echo = false) {
   $return = get_option($option);

   if($echo) {
      echo $return;
   } else {
      return $return;
   }
}

function atomic_option_font($fontID) {
   $fonts = array(
      array('id' => 0, 'title' => 'Alegreya Sans'),
      array('id' => 1, 'title' => 'Arimo'),
      array('id' => 2, 'title' => 'Arvo'),
      array('id' => 3, 'title' => 'Bitter'),
      array('id' => 4, 'title' => 'Cabin'),
      array('id' => 5, 'title' => 'Dosis'),
      array('id' => 6, 'title' => 'Droid Sans'),
      array('id' => 7, 'title' => 'Droid Serif'),
      array('id' => 8, 'title' => 'EB Garamond'),
      array('id' => 9, 'title' => 'Hind'),
      array('id' => 10, 'title' => 'Indie Flower'),
      array('id' => 11, 'title' => 'Lato'),
      array('id' => 12, 'title' => 'Merriweather'),
      array('id' => 13, 'title' => 'Montserrat'),
      array('id' => 14, 'title' => 'Noto Sans'),
      array('id' => 15, 'title' => 'Nunito'),
      array('id' => 16, 'title' => 'Open sans'),
      array('id' => 17, 'title' => 'Oswald'),
      array('id' => 18, 'title' => 'Oxygen'),
      array('id' => 19, 'title' => 'Pacifico'),
      array('id' => 20, 'title' => 'Poiret One'),
      array('id' => 21, 'title' => 'PT Sans'),
      array('id' => 22, 'title' => 'Raleway'),
      array('id' => 23, 'title' => 'Roboto'),
      array('id' => 24, 'title' => 'Titillium Web'),
      array('id' => 25, 'title' => 'Ubuntu')
   );

   return array('title' => $fonts[$fontID]['title'], 'name' => urlencode($fonts[$fontID]['title']));
}
/*---------------------------------------------------
Misc.
----------------------------------------------------*/

function addhttp($url) {
   if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
      $url = "http://" . $url;
   }
   return $url;
}

/**
* Dimox Breadcrumbs
* http://dimox.net/wordpress-breadcrumbs-without-a-plugin/
* Since ver 1.0
* Add this to any template file by calling dimox_breadcrumbs()
* Changes: MC added taxonomy support
*/
function dimox_breadcrumbs(){
   /* === OPTIONS === */
   $text['home']     = __('Home', 'galaxystreet'); // text for the 'Home' link
   $text['category'] = 'Archive by Category "%s"'; // text for a category page
   $text['tax'] 	  = 'Archive for "%s"'; // text for a taxonomy page
   $text['search']   = 'Search Results for "%s" Query'; // text for a search results page
   $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
   $text['author']   = 'Articles Posted by %s'; // text for an author page
   $text['404']      = 'Error 404'; // text for the 404 page
   $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
   $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
   $delimiter   = ' &raquo; '; // delimiter between crumbs
   $before      = '<span class="current">'; // tag before the current crumb
   $after       = '</span>'; // tag after the current crumb
   /* === END OF OPTIONS === */
   global $post;
   $homeLink = get_bloginfo('url') . '/';
   $linkBefore = '<span typeof="v:Breadcrumb">';
   $linkAfter = '</span>';
   $linkAttr = ' rel="v:url" property="v:title"';
   $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
   if (is_home() || is_front_page()) {
      if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';
   } else {
      echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

      if ( is_category() ) {
         $thisCat = get_category(get_query_var('cat'), false);
         if ($thisCat->parent != 0) {
            $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
         }
         echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
      } elseif( is_tax() ){
         $thisCat = get_category(get_query_var('cat'), false);
         if ($thisCat->parent != 0) {
            $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
         }
         echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;

      }elseif ( is_search() ) {
         echo $before . sprintf($text['search'], get_search_query()) . $after;
      } elseif ( is_day() ) {
         echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
         echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
         echo $before . get_the_time('d') . $after;
      } elseif ( is_month() ) {
         echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
         echo $before . get_the_time('F') . $after;
      } elseif ( is_year() ) {
         echo $before . get_the_time('Y') . $after;
      } elseif ( is_single() && !is_attachment() ) {
         if ( get_post_type() != 'post' ) {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
         } else {
            $cat = get_the_category(); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, $delimiter);
            if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
            if ($showCurrent == 1) echo $before . get_the_title() . $after;
         }
      } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
         $post_type = get_post_type_object(get_post_type());
         echo $before . $post_type->labels->singular_name . $after;
      } elseif ( is_attachment() ) {
         $parent = get_post($post->post_parent);
         $cat = get_the_category($parent->ID); $cat = $cat[0];
         $cats = get_category_parents($cat, TRUE, $delimiter);
         $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
         $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
         echo $cats;
         printf($link, get_permalink($parent), $parent->post_title);
         if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
      } elseif ( is_page() && !$post->post_parent ) {
         if ($showCurrent == 1) echo $before . get_the_title() . $after;
      } elseif ( is_page() && $post->post_parent ) {
         $parent_id  = $post->post_parent;
         $breadcrumbs = array();
         while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
            $parent_id  = $page->post_parent;
         }
         $breadcrumbs = array_reverse($breadcrumbs);
         for ($i = 0; $i < count($breadcrumbs); $i++) {
            echo $breadcrumbs[$i];
            if ($i != count($breadcrumbs)-1) echo $delimiter;
         }
         if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
      } elseif ( is_tag() ) {
         echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
      } elseif ( is_author() ) {
         global $author;
         $userdata = get_userdata($author);
         echo $before . sprintf($text['author'], $userdata->display_name) . $after;
      } elseif ( is_404() ) {
         echo $before . $text['404'] . $after;
      }
      if ( get_query_var('paged') ) {
         if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
         echo __('Page') . ' ' . get_query_var('paged');
         if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
      }
      echo '</div>';
   }
} // end dimox_breadcrumbs()
