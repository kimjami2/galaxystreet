<?PHP
/*---------------------------------------------------
Slides
----------------------------------------------------*/
function custom_post_slides() {
  $labels = array(
    'name'               => _x( 'Slides', 'post type general name' ),
    'singular_name'      => _x( 'Slide', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Product' ),
    'edit_item'          => __( 'Edit Slide' ),
    'new_item'           => __( 'New Slide' ),
    'all_items'          => __( 'All Slides' ),
    'view_item'          => __( 'View Slide' ),
    'search_items'       => __( 'Search Slides' ),
    'not_found'          => __( 'No slides found' ),
    'not_found_in_trash' => __( 'No slides found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Slides'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Create slides to be shown at the front page',
    'public'        => true,
    'menu_position' => 5,
    'menu_icon'     => 'dashicons-images-alt',
    'supports'      => array('title', 'editor', 'thumbnail'),
    'has_archive'   => true,
  );
  register_post_type( 'slide', $args );
}
add_action( 'init', 'custom_post_slides' );

/*---------------------------------------------------
Testimonials
----------------------------------------------------*/
function custom_post_testimonials() {
  $labels = array(
    'name'               => _x( 'Testimonials', 'post type general name' ),
    'singular_name'      => _x( 'Testimonial', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'bTestimonial' ),
    'add_new_item'       => __( 'Add New Testimonial' ),
    'edit_item'          => __( 'Edit Testimonial' ),
    'new_item'           => __( 'New Testimonial' ),
    'all_items'          => __( 'All Testimonials' ),
    'view_item'          => __( 'View Testimonial' ),
    'search_items'       => __( 'Search Testimonials' ),
    'not_found'          => __( 'No testimonials found' ),
    'not_found_in_trash' => __( 'No testimonials found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Testimonials'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Create testimonials to be shown at the front page',
    'public'        => true,
    'menu_position' => 5,
    'menu_icon'     => 'dashicons-format-quote',
    'supports'      => array('title', 'editor', 'thumbnail'),
    'has_archive'   => true,
  );
  register_post_type( 'testimonials', $args );
}
add_action( 'init', 'custom_post_testimonials' );


// meta boxes
function testimonials_author_metabox() {
   add_meta_box(
       'testminoal_author',
       'Author',
       'show_testimonials_author_metabox',
       'testimonials',
       'normal',
       'high'
   );
}

function show_testimonials_author_metabox() {
    global $post;

  	// Noncename needed to verify where the data originated
  	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    $name = get_post_meta($post->ID, 'testimonial_name', true);
    $title = get_post_meta($post->ID, 'testimonial_title', true);

    ?>

    <span class="title">Name</span>
    <input type="text" name="testimonial_name" class="widefat" value="<?php echo $name; ?>">
    <br /><br />
    <span class="title">Title</span>
    <input type="text" name="testimonial_title" class="widefat" value="<?php echo $title; ?>">

    <?php
}
add_action('add_meta_boxes', 'testimonials_author_metabox');

// save testmonial author
function save_testimonials_author_metabox($post_id, $post) {

  if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
  }

  if ( !current_user_can( 'edit_post', $post->ID )) {
    return $post->ID;
  }

    $events_meta['testimonial_name'] = $_POST['testimonial_name'];
    $events_meta['testimonial_title'] = $_POST['testimonial_title'];

    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
      if( $post->post_type == 'revision' ) return; // Don't store custom data twice
      $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
      if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
        update_post_meta($post->ID, $key, $value);
      } else { // If the custom field doesn't have a value
        add_post_meta($post->ID, $key, $value);
      }
      if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}

add_action('save_post', 'save_testimonials_author_metabox', 1, 2);

/*---------------------------------------------------
Gallery
----------------------------------------------------*/
add_action( 'init', 'create_gallery_categories', 0 );

function create_gallery_categories() {

	$labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Catagory', 'taxonomy singular name' ),
		'search_items'      => __( 'Search categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Catagory' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category' ),
		'menu_name'         => __( 'Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery_categories' ),
	);

  register_taxonomy( 'gallery_categories', array( 'gallery' ), $args );

}


function custom_post_gallery() {
  $labels = array(
    'name'               => _x( 'Gallery', 'post type general name' ),
    'singular_name'      => _x( 'Gallery', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Image' ),
    'edit_item'          => __( 'Edit Image' ),
    'new_item'           => __( 'New Image' ),
    'all_items'          => __( 'All Images' ),
    'view_item'          => __( 'View Image' ),
    'search_items'       => __( 'Search Image' ),
    'not_found'          => __( 'No images found' ),
    'not_found_in_trash' => __( 'No images found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Gallery'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Upload images to gallery page',
    'public'        => true,
    'menu_position' => 5,
    'menu_icon'     => 'dashicons-format-image',
    'supports'      => array('title', 'editor', 'thumbnail'),
    'has_archive'   => true,
    'taxonomies'    => array('gallery_categories'),
  );
  register_post_type( 'gallery_images', $args );
}
add_action( 'init', 'custom_post_gallery' );

/*---------------------------------------------------
Custom page fields
----------------------------------------------------*/
function adding_custom_meta_boxes( $post_type, $post ) {
    add_meta_box(
        'my-meta-box',
        __( 'My Meta Box' ),
        'render_my_meta_box',
        'post',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes', 'adding_custom_meta_boxes', 10, 2 );
