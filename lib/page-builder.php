<?php

/*
* Remove margin bottom
*/
function ap_pb_remove_margin() {
   add_theme_support( 'siteorigin-panels', array(
   	'margin-bottom' => 0,
   	'responsive' => true,
   ));
}

add_action('after_setup_theme', 'ap_pb_remove_margin');
/*
* Add Atomic Pixel specific category
*/
function ap_add_widget_tabs($tabs) {
    $tabs[] = array(
        'title' => __('Atomic Pixel GalaxyStreet', 'galaxystreet'),
        'filter' => array(
            'groups' => array('galaxystreet')
        )
    );

    return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', 'ap_add_widget_tabs', 20);

/*
* Atomic Pixel spefici css
*/
add_action('admin_head', 'ap_logo_icon');

function ap_logo_icon() {
  echo '
  <style>
      .ap-logo-icon {
         background-size: cover;
         background-image: url(../img/ap_logo_40_40.png);
      }

      .ap-widget-input-label {
         display: block;
         font-weight: bold;
         color: 3d3d3d;
         padding: 10px 0 5px 0;
      }

      .ap-tinymce .wp-editor-wrap {
         margin-top: -12px;
      }



      .ap-tinymce .wp-editor-wrap .mce-menubar {
         display: none;
      }

      .ap-tinymce .widefat {
         padding: 5px;
         box-shadow: 0;
      }

      .ap-widget-margin-top {
        margin-top: 25px;
        margin-bottom: 25px;
      }
  </style>';
};
