<?PHP
/*---------------------------------------------------
Bootstrap & co
----------------------------------------------------*/

function add_bs_shortcodes() {
  $bootstrapShortcodes = array(
    'row',
    'one-column',
    'two-column',
    'three-column',
    'four-column',
    'clear-columns',
    'tabs',
    'tab',
    'toggle',
    'button',
    'google-maps',
    'youtube',
  );
  foreach($bootstrapShortcodes as $shortcode) {
    $function = 'bs_' . str_replace('-', '_', $shortcode);
    add_shortcode($shortcode, $function);
  }
}

function bs_youtube($atts, $content) {

  $parseUrl = parse_url($content);
  $youtube = explode('=', $parseUrl['query']);

  $html = '<div class="youtube-frame"><iframe title="YouTube video player" src="https://www.youtube.com/embed/'.$youtube[1].'" width="560" height="315" frameborder="0"></iframe></div>';

  return $html;
}

function bs_google_maps($atts, $content) {

  $mapsId = 'google-maps-'.md5($content);

  $html = '<div id="'.$mapsId.'" class="google-maps">asd</div>
  <script type="text/javascript">
  $(document).ready(function() {
    var address = "'.$content.'";
    $.ajax({
      url: "http://maps.googleapis.com/maps/api/geocode/json?address="+address+",
      type: "POST",
      success: function(res){
       var mapOptions = {
         zoom: 15,
         scrollwheel: false,
         draggable: false,
         center: new google.maps.LatLng(res.results[0].geometry.location.lat, res.results[0].geometry.location.lng),
         styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}]

       };
       var mapElement = document.getElementById("'.$mapsId.'");
       var map = new google.maps.Map(mapElement, mapOptions);
       var marker = new google.maps.Marker({
         position: new google.maps.LatLng(res.results[0].geometry.location.lat, res.results[0].geometry.location.lng),
         map: map,
         title: ""
       });
      }
    });
  });
  </script>
  ';

  return $html;
}

function bs_row($atts, $content) {
  $html = '
    <div class="row">
      '.do_shortcode($content).'
    </div>
  ';
  return $html;
}

function bs_one_column($atts, $content) {
  $html = '
    <div class="col col-md-3 as-col">
      '.do_shortcode($content).'
    </div>
  ';
  return $html;
}

function bs_two_column($atts, $content) {
  $html = '
    <div class="col col-md-6 as-col">
      '.do_shortcode($content).'
    </div>
  ';
  return $html;
}

function bs_three_column($atts, $content) {
  $html = '
    <div class="col col-md-9 as-col">
      '.do_shortcode($content).'
    </div>
  ';
  return $html;
}

function bs_four_column($atts, $content) {
  $html = '
    <div class="col col-md-12 as-col">
      '.do_shortcode($content).'
    </div>
  ';
  return $html;
}

function bs_clear_columns($atts, $content) {
  $html = '
    <div class="clearfix"></div>
  ';
  return $html;
}

function bs_tabs($atts, $content = null) {
  if(isset($GLOBALS['tabs_count']))
  {
    $GLOBALS['tabs_count']++;
  }
  else {
    $GLOBALS['tabs_count'] = 0;
    $GLOBALS['tabs_default_count'] = 0;

    $a = shortcode_atts(array(
      'data' => false
    ), $atts);
  }

  $div_id       = 'custom-tabs-'.$GLOBALS['tab_count'];
  $atts_map     = bs_attribute_map($content);

  if ($atts_map) {
    $tabs = array();
    $GLOBALS['tabs_default_active'] = true;
    foreach( $atts_map as $check ) {
      if( !empty($check["tab"]["active"]) ) {
        $GLOBALS['tabs_default_active'] = false;
      }
    }
    $i = 0;
    foreach( $atts_map as $tab ) {
      $class  = ( !empty($tab["tab"]["active"]) || ($GLOBALS['tabs_default_active'] && $i == 0) ) ? 'active' : '';
      $tabs[] = '<li class="'.$class.'" aria-expanded="false"><a href="#custom-tab-'.$GLOBALS['tabs_count'].'-'.md5($tab['tab']['title']).'" data-toggle="tab">'.$tab['tab']['title'].'</a>';
      $i++;
    }
  }

  $tabsHTML = '<ul class="nav nav-tabs" id="'.$div_id.'">'.implode($tabs).'</ul><div class="tab-content">'.do_shortcode($content).'</div>';

  // remove breaks
  $output = str_replace(array('<br />'), '', $tabsHTML);

  return $output;
}

function bs_tab( $atts, $content = null ) {
  $atts = shortcode_atts(array(
    'title'   => false,
    'active'  => false,
    'data'    => false
  ), $atts);

  if($GLOBALS['tabs_default_active'] && $GLOBALS['tabs_default_count'] == 0) {
    $atts['active'] = true;
  }
  $GLOBALS['tabs_default_count']++;
  $active = ($atts['active'] == 'true') ? ' active' : '';
  $id     = 'custom-tab-'.$GLOBALS['tabs_count'].'-'.md5($atts['title']);

  $tabHTML = '<div id="'.$id.'" class="tab-pane '.$active.'">'.do_shortcode($content).'</div>';

  // remove breaks
  $output = str_replace('<br />', '', $tabHTML);

  return $output;

}

function bs_attribute_map($str, $att = null) {
  $res = array();
  $return = array();
  $reg = get_shortcode_regex();
  preg_match_all('~'.$reg.'~',$str, $matches);
  foreach($matches[2] as $key => $name) {
    $parsed = shortcode_parse_atts($matches[3][$key]);
    $parsed = is_array($parsed) ? $parsed : array();
    $res[$name] = $parsed;
    $return[] = $res;
  }
  return $return;
}

function bs_toggle($atts, $content) {
  $atts = shortcode_atts(array(
    'title'   => ''
  ), $atts);

  $id = 'toggle-'.md5($atts['title'].$content);

  return '
    <div class="panel panel-default">
      <div class="panel-heading" role="tab">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#'.$id.'" aria-expanded="true" aria-controls="collapseOne">'.$atts['title'].'</a>
        </h4>
      </div>
      <div id="'.$id.'" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">'.$content.'</div>
      </div>
    </div>';
}

function bs_button($atts, $content) {
  $atts = shortcode_atts(array(
    'link' => '#',
    'size' => 'medium',
    'type' => 'primary'
  ), $atts);

  return '<a class="btn btn-'.$atts['type'].' '.$atts['size'].'">'.$content.'</a>';
}

add_bs_shortcodes();
