<?php
add_action('wp_head','pluginname_ajaxurl');

function pluginname_ajaxurl() {
?>
  <script type="text/javascript">
  var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
  </script>
<?php
}


add_action( 'wp_ajax_nearby_places', 'nearby_places_callback' );
add_action( 'wp_ajax_nopriv_nearby_places', 'nearby_places_callback' );

function nearby_places_callback() {
	global $wpdb;

  // categories
  $categories = array('airport', 'amusement_park', 'art_gallery', 'bakery', 'bar', 'beauty_salon', 'bowling_alley', 'bus_station', 'cafe', 'casino', 'clothing_store', 'doctor', 'furniture_store', 'gym', 'hospital', 'library', 'movie_theater', 'museum', 'night_club', 'park', 'restaurant', 'school', 'spa', 'trainstation', 'university', 'zoo');

  $colors = array(
    'black' => '#3d3d3d',
    'purple' => '#8F3FAF',
    'grey' => '#33495F',
    'green' => '#1FCE6D',
    'orange' => '#E87E04',
    'yellow' => '#F2C500',
    'blue' => '#2C97DE',
    'red' => '#E94B35'
  );

  $colorCategories = array(
    'black' => array(
      'airport',
      'bus_station',
      'trainstation'
    ),
    'purple' => array(
      'restaurant',
      'bakery',
      'night_club'
    ),
    'grey' => array(
      'bar'
    ),
    'green' => array(
      'gym',
      'spa'
    ),
    'orange' => array(
      'amusement_park',
      'museum',
      'art_gallery',
      'bowling_alley',
      'beauty_salon',
      'casino',
      'library',
      'movie_theater',
      'zoo',
      'park'
    ),
    'yellow' => array(
      'school',
      'university'
    ),
    'blue' => array(
      'furniture_store',
      'clothing_store',
      'cafe'
    ),
    'red' => array(
      'doctor',
      'hospital'
    )
  );

  // create our exclude string
  foreach($categories as $category) {
    $places_exclude .= $category.'|';
  }

  // fetch nearby places from google search places
  $lat_lng = array('lat' => $_POST['latLng']['results'][0]['geometry']['location']['lat'], 'lng' => $_POST['latLng']['results'][0]['geometry']['location']['lng']);
  $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$lat_lng['lat'].','.$lat_lng['lng'].'&radius='.$_POST['options']['radius'].'&types='.$places_exclude.'&key='.$_POST['options']['key'].'&language='.$_POST['options']['language'];

  // curl
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  $nearby_places = json_decode(curl_exec($curl), true);
  curl_close($curl);

  // limit nearby_places
  $nearby_places['results'] = array_slice($nearby_places['results'],0,$_POST['options']['limit']);

  // set default type of places and colors
  for($i = 0; $i < count($nearby_places['results']); $i++) {
    $foundCategory = false;
    foreach($nearby_places['results'][$i]['types'] as $type) {
      if(in_array($type, $categories) && !$foundCategory) {
        $nearby_places['results'][$i]['type'] = $type;
        $foundCategory = true;
        $foundColor = false;

        // grab the color
        foreach($colorCategories as $color => $colorCategory) {
          if(in_array($type, $colorCategory) && !$foundColor) {
            $nearby_places['results'][$i]['color'] = $colors[$color];
            $foundColor = true;
          }
        }
      }
    }
  }

  echo json_encode($nearby_places);


	wp_die();
}
