<section class="error-page">

	<div class="alert alert-warning" style="text-align: center; margin-bottom: 80px;">
	  <?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?>
	</div>

</section>
