<?php
if(atomic_option('atomic_logo_use_image', false) == 'true' && !empty(atomic_option('atomic_logo_image_logo', false))) {
	$size = getimagesize(atomic_option('atomic_logo_image_logo', false));
	$logoHeightPx = $size[1]+50;
	$logoHeight = 'style="margin-top: '.$logoHeightPx.'px;"';
}
?>

<div <?php echo $logoHeight; ?>></div>
