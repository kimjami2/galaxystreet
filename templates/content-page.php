<div class="title-desc page-title">
  <h2><?php echo the_title(); ?></h2>
  <div class="line"></div>
</div>
<div class="single-page">
  <?php the_content(); ?>
</div>
<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
