<?php
$the_post = get_post();
?>

<div class="row">
  <div class="col-md-9">
    <?php
$my_postid = $the_post->ID;
$content_post = get_post($my_postid);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
echo $content;
?>
    <br />
    <time class="updated" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time>
  </div>
  <div class="col-md-3">
    <div class="sidebar-blog">
    <?php dynamic_sidebar('sidebar-primary'); ?>
    </div>
  </div>
</div>
