<!-- loading page effect -->
<div class="loading">
	<div id='loader'>
	  <div class='diamond'></div>
	  <div class='diamond'></div>
	  <div class='diamond'></div>
	</div>
</div>
<!-- /loading -->

<header id="header" class="container-fluid">

	<div class="mobile-menu">
		<?php
		if (has_nav_menu('primary_navigation')) {
			wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'menu', 'echo'=>true));
		}
		?>
	</div>

	<div class="container">

		<?php if(atomic_option('atomic_header_top_info') == 'true' || atomic_option('atomic_header_top_social') == 'true') { ?>

		<!-- top info -->
		<div class="top-info clearfix">

			<?php if(atomic_option('atomic_header_top_info') == 'true') { ?>

			<div class="contact-info col-md-8">
				<?php if(!empty(atomic_option('atomic_location_address'))) { ?>
				<p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?PHP ucfirst(atomic_option('atomic_location_address', true)); ?> <?PHP ucfirst(atomic_option('atomic_location_state', true)); ?><?php if(!empty(atomic_option('atomic_location_zip'))) { ?>, <?php atomic_option('atomic_location_zip', true);} ?></p>
				<?php } ?>
				<?php if(!empty(atomic_option('atomic_agent_phone'))) { ?>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;<?php atomic_option('atomic_agent_phone', true); ?></p>
				<?php } ?>
			</div>

			<?php } ?>

			<?php if(atomic_option('atomic_header_top_social') == 'true') { ?>

			<div class="social col-md-4 pull-right">
				<?php if(!empty(get_option('atomic_social_facebook'))) { ?>
				<a class="icon-button facebook" href="<?php echo addhttp(get_option('atomic_social_facebook')); ?>" target="new"><i class="mdi mdi-facebook"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_twitter'))) { ?>
				<a class="icon-button twitter" href="<?php echo addhttp(get_option('atomic_social_twitter')); ?>" target="new"><i class="mdi mdi-twitter"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_tumblr'))) { ?>
				<a class="icon-button tumblr" href="<?php echo addhttp(get_option('atomic_social_tumblr')); ?>" target="new"><i class="fa fa-tumblr"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_googleplus'))) { ?>
				<a class="icon-button google-plus" href="<?php echo addhttp(get_option('atomic_social_googleplus')); ?>" target="new"><i class="fa fa-google-plus"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_vimeo'))) { ?>
				<a class="icon-button vimeo" href="<?php echo addhttp(get_option('atomic_social_vimeo')); ?>" target="new"><i class="mdi mdi-vimeo"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_youtube'))) { ?>
				<a class="icon-button youtube" href="<?php echo addhttp(get_option('atomic_social_youtube')); ?>" target="new"><i class="fa fa-youtube"></i><span></span></a>
				<?php } ?>
				<?php if(!empty(get_option('atomic_social_pinterest'))) { ?>
				<a class="icon-button pinterest" href="pinterest.com" target="new"><i class="mdi mdi-pinterest"></i><span></span></a>
				<?php } ?>
			</div>

			<?php } ?>

		</div>
		<!-- /top info -->

		<?php } ?>

		<div class="seperator"></div>

		<!-- navbar -->
		<nav class="clearfix">
			<div class="logo-container">

				<?php if(atomic_option('atomic_logo_use_image') == 'true') { ?>

					<?php
						// check if we should override the default logo image dimensions
						$image_logo_width 	= atomic_option('atomic_logo_image_width');
						$image_logo_height 	= atomic_option('atomic_logo_image_height');

						$image_logo_width_css 	= (!empty($image_logo_width)) ? 'width: '.$image_logo_width.'px;' : '';
						$image_logo_height_css 	= (!empty($image_logo_height)) ? 'height: '.$image_logo_height.'px;' : '';
					?>

					<a href="<?php echo home_url(); ?>" class="logo light" style="background-image: url(<?php atomic_option('atomic_logo_image_logo', true); ?>); <?php echo $image_logo_width_css; ?> <?php echo $image_logo_height_css; ?>"></a>
					<a href="<?php echo home_url(); ?>" class="logo dark" style="background-image: url(<?php atomic_option('atomic_logo_image_logo_dark', true); ?>); <?php echo $image_logo_width_css; ?> <?php echo $image_logo_height_css; ?>"></a>

				<?php } else { ?>

					<a class="logo-text" href="<?php echo home_url(); ?>"><?php atomic_option('atomic_logo_text', true); ?></a>

				<?php } ?>

			</div>

			<div class="nav-wrapper">
			<?php
			if (has_nav_menu('primary_navigation')) {
				wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav', 'echo'=>true));
			}
			?>
			</div>

			<div class="mobile-menu-nav">
				<div class="back pull-left">
					<button class="back"></button>
				</div>
				<div class="menu pull-right">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>


		</nav>
		<!-- /navbar -->

	</div>


</header>
