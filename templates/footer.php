<?php
/**
 * FOOTER
 */

$footerSidebarClass = 'col-sm-12';
$footerSidebars = wp_get_sidebars_widgets();
$numFooterSidebars = 0;

// Footer 1
if ($footerSidebars['sidebar-footer-1']) :
  $numFooterSidebars++;
endif;

// Footer 2
if ($footerSidebars['sidebar-footer-2']) :
  $numFooterSidebars++;
endif;

// Footer 3
if ($footerSidebars['sidebar-footer-3']) :
  $numFooterSidebars++;
endif;

if ($numFooterSidebars === 1) {
  $footerSidebarClass = 'col-sm-6 col-sm-push-3';
} else if ($numFooterSidebars === 2) {
  $footerSidebarClass = 'col-sm-4';
} else if ($numFooterSidebars === 3) {
  $footerSidebarClass = 'col-sm-4';
}
?>

<footer class="footer">
  <div class="container">
    <div class="row">

      <?php if ($footerSidebars['sidebar-footer-1']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-1'); ?>
        <div class="spacing visible-xs"></div>
      </div>
      <?php endif; ?>

      <?php if ($footerSidebars['sidebar-footer-2']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-2'); ?>
        <div class="spacing visible-xs"></div>
      </div>
      <?php endif; ?>

      <?php if ($footerSidebars['sidebar-footer-3']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-3'); ?>
      </div>
      <?php endif; ?>

    </div>
  </div>
</footer>
