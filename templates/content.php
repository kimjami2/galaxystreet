<article <?php post_class('col-md-6 blog-post'); ?>>
  <header>
    <?php if ( has_post_thumbnail() ) { ?>
    <a href="<?php the_permalink(); ?>" class="thumbnail">
    <?PHP the_post_thumbnail( 'large', array( 'class' => 'img-responsive' ) ); ?>
    </a>
    <?php } ?>
    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <div>
      <div>
      <?php get_template_part('templates/entry-meta'); ?>
        <div class="entry-summary">
          <?php
          $content = get_the_content();
          $trimmed_content = wp_trim_words( $content, 30, ' &hellip; <br /><div style=""><a class="btn small btn-primary" href="'. get_permalink() .'">'.__('läs mer', 'roots').'</a></div>' );
          echo $trimmed_content;
          ?>
        </div>
      </div>
    </div>
  </header>
</article>
