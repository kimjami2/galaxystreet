<div class="menu">

  <div class="button_container hidden-md hidden-lg" id="toggle">
    <span class="top"></span>
    <span class="middle"></span>
    <span class="bottom"></span>
  </div>

  <ul class="nav hidden-sm hidden-xs" id="nav">
    <div class="menu-meny-1-container">
      <ul id="menu-meny-1" class="nav">
        <li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://brevreklam.se/">Hem</a></li>
        <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://brevreklam.se/om-oss/">Om oss</a></li>
        <li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="http://brevreklam.se/nyheter/">Nyheter</a></li>
        <li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://brevreklam.se/kontakt/">Kontakt</a></li>
      </ul>
    </div>
  </ul>

  <div class="overlay" id="overlay">
    <nav class="overlay-menu">
      <div class="menu-meny-1-container">
        <ul id="menu-meny-2" class="">
          <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://brevreklam.se/">Hem</a></li>
          <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://brevreklam.se/om-oss/">Om oss</a></li>
          <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="http://brevreklam.se/nyheter/">Nyheter</a></li>
          <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://brevreklam.se/kontakt/">Kontakt</a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>
