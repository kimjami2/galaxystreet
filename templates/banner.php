<div id="banner">




  <div class="image-slider">
    <?php

    $args = array(
      'post_type' => 'slide',
      'post_status' => 'publish',
      'order' => 'ASC',
      'posts_per_page' => -1,
      'caller_get_posts'=> 1
    );

    $slides = new WP_Query($args);
    $i = 0;
    if($slides->have_posts()) {
      while ($slides->have_posts()) {
        $slides->the_post();
        ?>
        <div class="slide">
          <?PHP the_post_thumbnail('full', array('class' => 'rsImg rsMainSlideImage')); ?>
          <div class="container">
            <div>
              <div class="caption-wrapper">
                <h1><?php echo $slides->posts[$i]->post_title ?></h1>
                <p><?php echo $slides->posts[$i]->post_content ?></p>
              </div>
              <?php
              if(atomic_option('atomic_header_contact', false) == 'true') {
                ?>
                <div class="contact-now">
                  <div class="wrapper">
                    <p>
                      <?php atomic_option('atomic_header_contact_title', true); ?>
                    </p>
                    <btn class="btn btn-primary square"><?php atomic_option('atomic_header_contact_button_title', true); ?></btn>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
        <?php
        $i++;
      }
    }
    wp_reset_query();
    ?>
  </div>

</div>
