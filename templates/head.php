<!doctype html>
<html lang="sv" class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" />
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main.css" />
  <link rel="stylesheet" href="<?PHP echo get_stylesheet_directory_uri(); ?>/assets/css/materialize.min.css" />
  <link rel="stylesheet" href="<?PHP echo get_stylesheet_directory_uri(); ?>/assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?PHP echo get_stylesheet_directory_uri(); ?>/assets/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="<?PHP echo get_stylesheet_directory_uri(); ?>/assets/css/simple-image-slider.css" />
  <link rel="stylesheet" href="<?PHP echo get_stylesheet_directory_uri(); ?>/assets/css/map-icons.min.css" />

  <!-- Google Maps -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

  <?php wp_head(); ?>


  <style type="text/css">
    <?php include get_template_directory().'/style.php'; ?>
  </style>

    <script>
    <?php if(atomic_option('atomic_header_top_white', false) == 'true') { ?>
      var whiteHeader = true;
    <?php } else { ?>
      var whiteHeader = false;
    <?php } ?>

    </script>
  </head>
