<?php
/*
Plugin Name: Atomic Pixel GalaxyStreet House Info
Description: Display house info
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCHouseInfo {
  function __construct() {
    add_action('init', array($this, 'integrateWithVC'));
    add_shortcode('house_info', array($this, 'render_house_info'));
    add_action('wp_enqueue_scripts', array($this, 'loadCssAndJs'));
  }

  public function integrateWithVC() {

    // Check if Visual Composer is installed
    if (!defined( 'WPB_VC_VERSION')) {
      add_action('admin_notices', array($this, 'showVcVersionNotice'));
      return;
    }

    // Shortcode
    vc_map(array(
      "name" => __("House info", 'galaxystreet'),
      "description" => __("Show House info", 'galaxystreet'),
      "base" => "house_info",
      "class" => "",
      "controls" => "none",
      "weight" => 100,
      "icon" => 'vc_widget_icon',
      "category" => __('GalaxyStreet'),
      "custom_markup" => '<h4 class="wpb_element_title">
        <i class="vc_general vc_element-icon vc_widget_icon"></i>
        House info
      </h4>
      <span class="vc_admin_label admin_label_link">Show House info</span>
      ',
      "params" => array(
        array(
          "type" => "attach_image",
          "holder" => "div",
          "class" => "",
          "heading" => __("Background image", 'vc_extend'),
          "param_name" => "background_image",
          "description" => __("The background image of the section", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box title 1", 'vc_extend'),
          "param_name" => "box_title_1",
          "description" => __("The title of the first box", 'vc_extend')
        ),
        array(
          "type" => "textarea",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box content 1", 'vc_extend'),
          "param_name" => "box_content_1",
          "description" => __("The content of the first box", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box title 2", 'vc_extend'),
          "param_name" => "box_title_2",
          "description" => __("The title of the second box", 'vc_extend')
        ),
        array(
          "type" => "textarea",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box content 2", 'vc_extend'),
          "param_name" => "box_content_2",
          "description" => __("The content of the second box", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box title 3", 'vc_extend'),
          "param_name" => "box_title_3",
          "description" => __("The title of the third box", 'vc_extend')
        ),
        array(
          "type" => "textarea",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box content 3", 'vc_extend'),
          "param_name" => "box_content_3",
          "description" => __("The content of the third box", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box title 4", 'vc_extend'),
          "param_name" => "box_title_4",
          "description" => __("The title of the fourth box", 'vc_extend')
        ),
        array(
          "type" => "textarea",
          "holder" => "div",
          "class" => "",
          "heading" => __("Box content 4", 'vc_extend'),
          "param_name" => "box_content_4",
          "description" => __("The content of the fourth box", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Button title", 'vc_extend'),
          "param_name" => "button_title",
          "description" => __("The title of the button", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Button URL", 'vc_extend'),
          "param_name" => "button_url",
          "description" => __("The URL of the button", 'vc_extend')
        ),
      )
    ));
  }

  /*
  Shortcode logic how it should be rendered
  */
  public function render_house_info($atts, $content = null ) {
    extract(shortcode_atts(array(), $atts));

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
    $background_image = wp_get_attachment_image_src($atts['background_image'], 'original');

    /*
    * HTML
    */

    /*
    * HTML
    */
    $html = '
    <div class="house-info container-fluid gs-section image " style="background-image: url('.$background_image[0].');">
    <div class="row">
    <div class="info head col-lg-6 col-md-6 col-sm-12">
    <div class="wrapper">
    <div class="inner-wrapper">
    ';

    if(!empty($atts['box_content_1']) || !empty($atts['box_title_1'])) {
      $html .= '<div class="row additional">';
    }

    if(!empty($atts['box_content_1']) || !empty($atts['box_title_1'])) {
      $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
      $html .= !empty($atts['box_title_1']) ? '<h3>'.$atts['box_title_1'].'</h3>': '';
      $html .= !empty($atts['box_content_1']) ? '<p>'.$atts['box_content_1'].'</p>': '';
      $html .= '</div>';
    }

    if(!empty($atts['box_content_2']) || !empty($atts['box_title_2'])) {
      $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
      $html .= !empty($atts['box_title_2']) ? '<h3>'.$atts['box_title_2'].'</h3>': '';
      $html .= !empty($atts['box_content_2']) ? '<p>'.$atts['box_content_2'].'</p>': '';
      $html .= '</div>';
    }

    if(!empty($atts['box_content_3']) || !empty($atts['box_title_3'])) {
      $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
      $html .= !empty($atts['box_title_3']) ? '<h3>'.$atts['box_title_3'].'</h3>': '';
      $html .= !empty($atts['box_content_3']) ? '<p>'.$atts['box_content_3'].'</p>': '';
      $html .= '</div>';
    }

    if(!empty($atts['box_content_4']) || !empty($atts['box_title_4'])) {
      $html .= '<div class="col-md-6 col-sm-6 col-xs-12">';
      $html .= !empty($atts['box_title_4']) ? '<h3>'.$atts['box_title_4'].'</h3>': '';
      $html .= !empty($atts['box_content_4']) ? '<p>'.$atts['box_content_4'].'</p>': '';
      $html .= '</div>';
    }

    if(!empty($atts['box_content_1']) || !empty($atts['box_title_1'])) {
      $html .= '</div>';
    }

    if(!empty($atts['button_title'])) {
       $html .= '
       <div class="read-more">
       <a id="home-about-btn" href="'.addhttp($atts['button_url']).'" class="btn btn-pure-white medium scroll-prepare">'.$atts['button_title'].'</a>
       </div>';
    }

    $html .= '
    </div>
    </div>
    </div>
    </div>
    </div>
    ';

    return $html;
  }

  // JS/CSS
  public function loadCssAndJs() {
    wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    wp_enqueue_style( 'vc_extend_style' );
    //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
  }

  /*
  Show notice if VC is not present
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }
}

// Initialize code
new VCHouseInfo();
