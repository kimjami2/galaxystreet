<?php
/*
Plugin Name: Atomic Pixel GalaxyStreet Nearby places
Description: Display Nearby places
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCNearbyPlaces {
  function __construct() {
    add_action('init', array($this, 'integrateWithVC'));
    add_shortcode('nearby_places', array($this, 'render_nearby_places'));
    add_action('wp_enqueue_scripts', array($this, 'loadCssAndJs'));
  }

  public function integrateWithVC() {

    // Check if Visual Composer is installed
    if (!defined( 'WPB_VC_VERSION')) {
      add_action('admin_notices', array($this, 'showVcVersionNotice'));
      return;
    }

    // Shortcode
    vc_map(array(
      "name" => __("Nearby places", 'galaxystreet'),
      "description" => __("Show Nearby places", 'galaxystreet'),
      "base" => "nearby_places",
      "class" => "",
      "controls" => "none",
      "weight" => 100,
      "icon" => 'vc_widget_icon',
      "category" => __('GalaxyStreet'),
      "custom_markup" => '<h4 class="wpb_element_title">
        <i class="vc_general vc_element-icon vc_widget_icon"></i>
        Nearby places
      </h4>
      <span class="vc_admin_label admin_label_link">Show Nearby places</span>
      ',
      "params" => array(
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Address", 'vc_extend'),
          "param_name" => "address",
          "description" => __("The address of the house", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Max results", 'vc_extend'),
          "param_name" => "max_results",
          "description" => __("Max results of the nearby places", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Zoom", 'vc_extend'),
          "param_name" => "zoom",
          "description" => __("Zoom value (5 - 20)", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Radius", 'vc_extend'),
          "param_name" => "radius",
          "description" => __("The radius of the nearby places (200 - 1000)", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Language code", 'vc_extend'),
          "param_name" => "language_code",
          "description" => __('List of all supported language codes: <a target="_new" href="https://developers.google.com/maps/faq#languagesupport">Language codes</a>', 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Google API Key", 'vc_extend'),
          "param_name" => "api_key",
          "description" => __('Don\'t have an API key? Go to <a target="_new" href="https://developers.google.com/places/web-service/get-api-key">developers.google.com</a> and generate one.', 'vc_extend')
        ),
        array(
          "type" => "checkbox",
          "holder" => "div",
          "class" => "",
          "heading" => __("Theme background color", 'vc_extend'),
          "param_name" => "bg_color",
          "description" => __("Use the themes background color for the section", 'vc_extend')
        )
      )
    ));
  }

  /*
  Shortcode logic how it should be rendered
  */
  public function render_nearby_places($atts, $content = null ) {
    extract(shortcode_atts(array(), $atts));

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
    $bgcolor = !empty($atts['bg_color']) ? 'brand-background' : '';

    /*
    * HTML
    */

    $html .= '
    <div class="nearby-places container-fluid gs-section '.$bgcolor.'">
    <div class="container">
    <div class="row title-desc">
    <div class="col-md-8 col-md-push-2">
    <h2>Nearby places</h2><div class="line"></div>
    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
    </div>
    </div>
    <div class="row">
    <div class="map col-md-12">
    <div class="google-map" id="nearby-places-map" style="height: 560px;"></div>
    <div class="places clearfix">

    </div>
    </div>
    </div>
    </div>
    </div>

    <script>
    var surroundingsOptions = {
      "limit": '.($atts['max_results'] ? $atts['max_results'] : 10).',
      "zoom": '.($atts['zoom'] ? $atts['zoom'] : 17).',
      "radius": '.($atts['radius'] ? $atts['radius'] : 200).',
      "language": "'.($atts['language_code'] ? $atts['language_code'] : 'en').'",
      "key": "'.$atts['api_key'].'"
    };

    jQuery(document).ready(function() {
      GalaxyStreet.surroundings("'.urlencode($atts['address']).'");
    });
    </script>
    ';

    return $html;
  }

  // JS/CSS
  public function loadCssAndJs() {
    wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    wp_enqueue_style( 'vc_extend_style' );
    //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
  }

  /*
  Show notice if VC is not present
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }
}

// Initialize code
new VCNearbyPlaces();
