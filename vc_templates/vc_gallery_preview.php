<?php
/*
Plugin Name: Atomic Pixel GalaxyStreet Property Details
Description: Display property details
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCGalleryPreview {
  function __construct() {
    add_action('init', array($this, 'integrateWithVC'));
    add_shortcode('gallery_preview', array($this, 'render_gallery_preview'));
    add_action('wp_enqueue_scripts', array($this, 'loadCssAndJs'));
  }

  public function integrateWithVC() {

    // Check if Visual Composer is installed
    if (!defined( 'WPB_VC_VERSION')) {
      add_action('admin_notices', array($this, 'showVcVersionNotice'));
      return;
    }

    // Shortcode
    vc_map(array(
      "name" => __("Gallery preview", 'galaxystreet'),
      "description" => __("Show gallery images", 'galaxystreet'),
      "base" => "gallery_preview",
      "class" => "",
      "controls" => "none",
      "weight" => 100,
      "icon" => 'vc_widget_icon',
      "category" => __('GalaxyStreet'),
      "custom_markup" => '<h4 class="wpb_element_title">
        <i class="vc_general vc_element-icon vc_widget_icon"></i>
        Gallery preview
      </h4>
      <span class="vc_admin_label admin_label_link">Show gallery images</span>
      ',
      "params" => array(
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "admin_label" => true,
          "heading" => __("Title", 'vc_extend'),
          "param_name" => "title",
          "description" => __("The section title", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "admin_label" => false,
          "heading" => __("Description", 'vc_extend'),
          "param_name" => "description",
          "description" => __("The section description", 'vc_extend')
        ),
        array(
          "type" => "checkbox",
          "holder" => "div",
          "class" => "",
          "heading" => __("Theme background color", 'vc_extend'),
          "param_name" => "bg_color",
          "description" => __("Use the themes background color for the section", 'vc_extend')
        )
      )
    ));
  }

  /*
  Shortcode logic how it should be rendered
  */
  public function render_gallery_preview($atts, $content = null ) {
    extract(shortcode_atts(array(), $atts));

    $content = wpb_js_remove_wpautop($content, true);
    $bgcolor = !empty($atts['bg_color']) ? 'brand-background' : '';
    $taxonomies   = array('gallery_categories');
    $args         = array('orderby' => 'name','order' => 'ASC','hide_empty' => true,);

    /*
    * HTML
    */

    $html = '<div class="gallery-preview container-fluid gs-section '.$bgcolor.'">
    <div class="container-fluid">';

    // Title & Description
    if(!empty($atts['title']) || !empty($atts['description'])) {
      $html .= '
      <div class="row">
      <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-10 col-sm-push-1">';

      $html .= !empty($atts['title']) ? '<h2>'.$atts['title'].'</h2><div class="line"></div>' : false;
      $html .= !empty($atts['description']) ? '<p class="desc">'.$atts['description'].'</p>' : false;

      $html .= '
      </div>
      </div>';
    }

    // List all our images
    $html .= '<div class="row images">';

    $terms = get_terms($taxonomies, $args);
    foreach($terms as $key => $category) {

      $args = array(
        'post_type' => 'gallery_images',
        'gallery_categories' => $category->slug
      );

      $query = new WP_Query( $args );
      if($query->have_posts()) {
        $query->the_post();
        $countText = count($query->posts) > 0 ? 'Images' : 'Image';
        $html .= '
        <a href="'.$atts['url'].'?category='.$category->slug.'" class="preview col-lg-3 col-md-4 col-sm-6">
        <div class="col-md-12">
        <div class="overlay soft">
        <div class="icon brand-primary-color">
        <i class="fa fa-arrow-right"></i>
        </div>
        <div class="count">
        <p>'.count($query->posts).' '.$countText.'</p>
        </div>
        </div>
        <div class="title">
        <p>'.$category->name.'</p>
        </div>
        </div>
        <div class="image soft" style="background-image: url('. wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0] .');"></div>
        </a>
        ';

      }
      wp_reset_postdata();
    }

    $html .= '
    </div>
    <div class="row see-all-images clearfix">
    <a href="'.$a['galleryurl'].'" id="home-gallery-btn" class="btn btn-primary scroll-prepare">See all images</a>
    </div>
    </div>
    </div>
    ';

    return $html;
  }

  // JS/CSS
  public function loadCssAndJs() {
    wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    wp_enqueue_style( 'vc_extend_style' );
    //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
  }

  /*
  Show notice if VC is not present
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }
}

// Initialize code
new VCGalleryPreview();
