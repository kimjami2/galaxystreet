<?php
/*
Plugin Name: Atomic Pixel GalaxyStreet Property Details
Description: Display property details
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCPropertyDetails {
  function __construct() {
    add_action('init', array($this, 'integrateWithVC'));
    add_shortcode('property_details', array($this, 'render_property_details'));
    add_action('wp_enqueue_scripts', array($this, 'loadCssAndJs'));
  }

  public function integrateWithVC() {

    // Check if Visual Composer is installed
    if (!defined( 'WPB_VC_VERSION')) {
      add_action('admin_notices', array($this, 'showVcVersionNotice'));
      return;
    }

    // Shortcode
    vc_map(array(
      "name" => __("Property details", 'galaxystreet'),
      "description" => __("Show property details", 'galaxystreet'),
      "base" => "property_details",
      "class" => "",
      "controls" => "none",
      "weight" => 100,
      "icon" => 'vc_widget_icon',
      "category" => __('GalaxyStreet'),
      "custom_markup" => '<h4 class="wpb_element_title">
        <i class="vc_general vc_element-icon vc_widget_icon"></i>
        Property details
      </h4>
      <span class="vc_admin_label admin_label_link">Show property details</span>
      ',
      "params" => array(
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Title", 'vc_extend'),
          "param_name" => "title",
          "description" => __("The section title", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Description", 'vc_extend'),
          "param_name" => "description",
          "description" => __("The section description", 'vc_extend'),
          "admin_label" => false
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Sqft", 'vc_extend'),
          "param_name" => "sqft",
          "description" => __("The sqft of the property", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Sqft unit", 'vc_extend'),
          "param_name" => "sqft_unit",
          "description" => __("The sqft unit of the property", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Bedrooms", 'vc_extend'),
          "param_name" => "bedrooms",
          "description" => __("Number of bedrooms of the property", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Bathrooms", 'vc_extend'),
          "param_name" => "bathrooms",
          "description" => __("Number of bathrooms of the property", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Parkings", 'vc_extend'),
          "param_name" => "parkings",
          "description" => __("Number of parkings of the property", 'vc_extend')
        ),
        array(
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => __("Build year", 'vc_extend'),
          "param_name" => "build_year",
          "description" => __("The build year of the property", 'vc_extend')
        ),
        array(
          "type" => "checkbox",
          "holder" => "div",
          "class" => "",
          "heading" => __("Theme background color", 'vc_extend'),
          "param_name" => "bg_color",
          "description" => __("Use the themes background color for the section", 'vc_extend')
        )
      )
    ));
  }

  /*
  Shortcode logic how it should be rendered
  */
  public function render_property_details($atts, $content = null ) {
    extract(shortcode_atts(array(
      'title' => 'test',
      'description' => 'test'
    ), $atts));

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
    $bgcolor = !empty($atts['bg_color']) ? 'brand-background' : '';

    /*
    * HTML
    */

    // Javascript for counter
    $html = '
    <script type="text/javascript">
    var sqft = '.$atts["sqft"].';
    var bedrooms = '.$atts["bedrooms"].';
    var bathrooms = '.$atts["bathrooms"].';
    var parkings = '.$atts["parkings"].';
    var buildYear = '.$atts["build_year"].';
    </script>
    <div id="property-details" class="container-fluid clearfix gs-section '.$bgcolor.'">
    ';

    // Title & Description
    if(!empty($atts['title']) || !empty($atts['description'])) {
    $html .= '
    <div class="container">
    <div class="row">
    <div class="head col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-10 col-sm-push-1">';

    $html .= !empty($atts['title']) ? '<h2>'.$atts['title'].'</h2><div class="line"></div>' : false;
    $html .= !empty($atts['description']) ? '<p class="desc">'.$atts['description'].'</p>' : false;

    $html .= '
    </div>
    </div>
    </div>';
    }

    $html .= '<div class="items">';

    // check how many columns we need
    $countProperties = 1;

    if(empty($atts['sqft'])) {
      $countProperties++;
    }
    if(empty($atts['bedrooms'])) {
      $countProperties++;
    }
    if(empty($atts['bathrooms'])) {
      $countProperties++;
    }
    if(empty($atts['parkings'])) {
      $countProperties++;
    }
    if(empty($atts['build_year'])) {
      $countProperties++;
    }

    if(!empty($atts["sqft"])) {
      $html .= '
      <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
      <div class="icon">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
  	     viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
          <style type="text/css">
          	.st0{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
          	.st1{fill:none;stroke:#A6AAAE;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
          </style>
          <g>
          	<polyline class="st0" points="27.5,77.5 27.5,82.5 72.5,82.5 72.5,77.5 	"/>
          	<line class="st0" x1="42.5" y1="77.5" x2="42.5" y2="82.5"/>
          	<line class="st0" x1="57.5" y1="77.5" x2="57.5" y2="82.5"/>
          	<line class="st1" x1="31.2" y1="80" x2="31.2" y2="82.5"/>
          	<line class="st1" x1="35" y1="80" x2="35" y2="82.5"/>
          	<line class="st1" x1="38.8" y1="80" x2="38.8" y2="82.5"/>
          	<line class="st1" x1="46.2" y1="80" x2="46.2" y2="82.5"/>
          	<line class="st1" x1="50" y1="80" x2="50" y2="82.5"/>
          	<line class="st1" x1="53.8" y1="80" x2="53.8" y2="82.5"/>
          	<line class="st1" x1="61.2" y1="80" x2="61.2" y2="82.5"/>
          	<line class="st1" x1="65" y1="80" x2="65" y2="82.5"/>
          	<line class="st1" x1="68.8" y1="80" x2="68.8" y2="82.5"/>
          	<path class="st0" d="M27.5,40v31.2c0,0.7,0.6,1.3,1.2,1.3h42.5c0.7,0,1.2-0.6,1.2-1.3V40l10,10c0,0-32.5-32.5-32.5-32.5L17.5,50"/>
          </g>
        </svg>
      </div>
      <p class="desc"><span id="house-sqft">'.$atts["sqft"].'</span> '.$atts["sqft_unit"].'</p>
      <p class="title">'.__('Size', 'galaxystreet').'</p>
      </div>';
    }

    if(!empty($atts["bedrooms"])) {
      $html .= '
      <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
      <div class="icon">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="sofa" x="0px" y="0px"
        	 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
        <style type="text/css">
        	.st0{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        </style>
        <g>
        	<path class="st0" d="M25,31.9c-1.4,0-2.5,1.1-2.5,2.5v16.9h55V34.4c0-1.4-1.1-2.5-2.5-2.5H25z"/>
        	<path class="st0" d="M15,56.2c0-2.8,2.2-5,5-5h60c2.8,0,5,2.2,5,5"/>
        	<path class="st0" d="M85,56.2c0,2.8-2.2,5-5,5H20c-2.8,0-5-2.2-5-5"/>
        	<line class="st0" x1="22.5" y1="61.2" x2="22.5" y2="66.2"/>
        	<line class="st0" x1="26.2" y1="61.2" x2="26.2" y2="66.2"/>
        	<path class="st0" d="M26.2,66.2c0,1-0.8,1.9-1.9,1.9s-1.9-0.8-1.9-1.9"/>
        	<line class="st0" x1="73.8" y1="61.2" x2="73.8" y2="66.2"/>
        	<line class="st0" x1="77.5" y1="61.2" x2="77.5" y2="66.2"/>
        	<path class="st0" d="M77.5,66.2c0,1-0.8,1.9-1.9,1.9c-1,0-1.9-0.8-1.9-1.9"/>
        	<path class="st0" d="M31.9,51.2c-1.7,0-3.1-1.4-3.1-3.1s1.4-3.1,3.1-3.1h7.5c1.7,0,3.1,1.4,3.1,3.1s-1.4,3.1-3.1,3.1H31.9z"/>
        	<path class="st0" d="M60.6,51.2c-1.7,0-3.1-1.4-3.1-3.1s1.4-3.1,3.1-3.1h7.5c1.7,0,3.1,1.4,3.1,3.1s-1.4,3.1-3.1,3.1H60.6z"/>
        </g>
        </svg>
      </div>
      <p class="desc"><span id="house-bedrooms">'.$atts["bedrooms"].'</span></p>
      <p class="title">'.__('Bedrooms', 'galaxystreet').'</p>
      </div>';
    }

    if(!empty($atts["bathrooms"])) {
      $html .= '
      <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
      <div class="icon">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
        <style type="text/css">
        	.st0{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        	.st1{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-miterlimit:10;}
        	.st2{fill:none;stroke:#A6AAAE;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        </style>
        <g>
        	<g>
        		<path class="st0" d="M31.2,74.9c6.2,0,6.2,4,12.5,4c6.3,0,6.3-4,12.5-4s6.3,4,12.5,4"/>
        	</g>
        	<path class="st1" d="M46.2,31.9c0-2.1,1.6-3.8,3.7-3.8s3.8,1.6,3.8,3.7L46.2,31.9z"/>
        	<line class="st2" x1="53.7" y1="34.3" x2="55" y2="36.8"/>
        	<line class="st2" x1="56.2" y1="39.3" x2="57.5" y2="41.8"/>
        	<line class="st2" x1="46.2" y1="34.4" x2="45" y2="36.9"/>
        	<line class="st2" x1="43.7" y1="39.4" x2="42.5" y2="41.9"/>
        	<line class="st2" x1="49.9" y1="35.6" x2="50" y2="38.1"/>
        	<line class="st2" x1="50" y1="40.6" x2="50" y2="44.4"/>
        	<rect x="31.2" y="58.1" class="st0" width="7.5" height="5"/>
        	<line class="st0" x1="36.2" y1="58.1" x2="36.2" y2="24.4"/>
        	<line class="st0" x1="50" y1="24.4" x2="36.2" y2="24.4"/>
        	<line class="st0" x1="49.9" y1="28.1" x2="50" y2="24.4"/>
        	<path class="st0" d="M33.1,15.6c-1,0-1.9,0.8-1.9,1.9v65.1c0,0.8,0.5,1.5,1.3,1.8c0,0,34.2,0,34.4,0c1,0,1.9-0.8,1.9-1.9l0-65
        		c0-1-0.8-1.9-1.9-1.9H33.1z"/>
        </g>
        </svg>
      </div>
      <p class="desc"><span id="house-bathrooms">'.$atts["bathrooms"].'</span></p>
      <p class="title">'.__('Bathrooms', 'galaxystreet').'</p>
      </div>';
    }

    if(!empty($atts["parkings"])) {
      $html .= '
      <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
      <div class="icon">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        	 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
        <style type="text/css">
        	.st0{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        	.st1{fill:none;stroke:#A6AAAE;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        </style>
        <g>
        	<path class="st0" d="M27.5,45v31.2c0,0.7,0.6,1.3,1.2,1.3h42.5c0.7,0,1.2-0.6,1.2-1.3V45l10,10c0,0-32.5-32.5-32.5-32.5L17.5,55"/>
        	<polygon class="st0" points="33.8,47.5 27.5,62.5 72.5,62.5 66.2,47.5 	"/>
        	<polyline class="st0" points="31.2,52.5 31.2,47.5 68.8,47.5 68.8,52.5 	"/>
        	<line class="st0" x1="31.2" y1="62.5" x2="31.2" y2="77.5"/>
        	<line class="st0" x1="68.8" y1="62.5" x2="68.8" y2="77.5"/>
        	<line class="st1" x1="37.5" y1="51.2" x2="62.5" y2="51.2"/>
        	<line class="st1" x1="65" y1="55" x2="35" y2="55"/>
        	<line class="st1" x1="67.5" y1="58.8" x2="32.5" y2="58.8"/>
        </g>
        </svg>
      </div>
      <p class="desc"><span id="house-parkings">'.$atts["parkings"].'</span></p>
      <p class="title">'.__('Parking', 'galaxystreet').'</p>
      </div>';
    }

    if(!empty($atts["build_year"])) {
      $html .= '
      <div class="item col-lg-2 col-lg-push-'.$countProperties.' col-md-4 col-sm-6 col-xs-6">
      <div class="icon">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="hammer" x="0px" y="0px"
        	 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
        <style type="text/css">
        	.st0{fill:none;stroke:#A6AAAE;stroke-width:1.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
        </style>
        <g>
        	<rect x="38.4" y="31.9" transform="matrix(0.9397 0.342 -0.342 0.9397 21.5599 -11.0519)" class="st0" width="7.5" height="47.5"/>
        	<path class="st0" d="M60.4,23.7c-0.5,1.3-1.9,2-3.2,1.5l-11.7-4.3c-1-0.4-2,0.1-2.4,1.1l-2.1,5.9c-0.4,1,0.1,2,1.1,2.4l11.7,4.3
        		c1.3,0.5,2,1.9,1.5,3.2"/>
        	<path class="st0" d="M55.3,37.8c-0.5,1.3,0.2,2.7,1.5,3.2l4.7,1.7c1,0.4,2-0.1,2.4-1.1l5.6-15.3c0.4-1-0.1-2-1.1-2.4l-4.7-1.7
        		c-1.3-0.5-2.7,0.2-3.2,1.5"/>
        </g>
        </svg>
      </div>
      <p class="desc"><span id="house-build-year">'.$atts["build_year"].'</span></p>
      <p class="title">'.__('Build Year', 'galaxystreet').'</p>
      </div>';
    }

    $html .= '
    </div>
    </div>';

    return $html;
  }

  // JS/CSS
  public function loadCssAndJs() {
    wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    wp_enqueue_style( 'vc_extend_style' );
    //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
  }

  /*
  Show notice if VC is not present
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }
}

// Initialize code
new VCPropertyDetails();
