<?php
/*
Plugin Name: Atomic Pixel GalaxyStreet Testimonials
Description: Display Testimonials
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCTestimonials {
  function __construct() {
    add_action('init', array($this, 'integrateWithVC'));
    add_shortcode('testimonials', array($this, 'render_testimonials'));
    add_action('wp_enqueue_scripts', array($this, 'loadCssAndJs'));
  }

  public function integrateWithVC() {

    // Check if Visual Composer is installed
    if (!defined( 'WPB_VC_VERSION')) {
      add_action('admin_notices', array($this, 'showVcVersionNotice'));
      return;
    }

    // Shortcode
    vc_map(array(
      "name" => __("Testimonials", 'galaxystreet'),
      "description" => __("Show Testimonials", 'galaxystreet'),
      "base" => "testimonials",
      "class" => "",
      "controls" => "none",
      "weight" => 100,
      "icon" => 'vc_widget_icon',
      "category" => __('GalaxyStreet'),
      "custom_markup" => '<h4 class="wpb_element_title">
        <i class="vc_general vc_element-icon vc_widget_icon"></i>
        Testimonials
      </h4>
      <span class="vc_admin_label admin_label_link">Show Testimonials</span>
      ',
      "params" => array(
        array(
          "type" => "attach_image",
          "holder" => "div",
          "class" => "",
          "heading" => __("Background image", 'vc_extend'),
          "param_name" => "background_image",
          "description" => __("The background image of the section", 'vc_extend')
        )
      )
    ));
  }

  /*
  Shortcode logic how it should be rendered
  */
  public function render_testimonials($atts, $content = null ) {
    extract(shortcode_atts(array(), $atts));

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
    $background_image = wp_get_attachment_image_src($atts['background_image'], 'original');

    /*
    * HTML
    */

    $html = '
    <div class="home-testimonials parallax-window" data-parallax="scroll" data-image-src="'.$background_image[0].'">
    <div class="container-fluid ani">

    <div class="home-testimonials-info row info scroll-prepare">
      <div class="testimonials-slide">';


    $args=array(
      'post_type' => 'testimonials',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'caller_get_posts'=> 1);

      $testimonials = new WP_Query($args);
      if($testimonials->have_posts()) {

        $testimonials->the_post();

        foreach($testimonials->posts as $post) {
          $meta = get_post_meta($post->ID);
          $html .= '
          <div class="slide row">
          <div class="caption-wrapper">
          <div class="info col-lg-6 col-lg-push-3 col-md-6 col-md-push-3">
          <div class="icon scroll-prepare"></div>
          <p class="quote">'.$post->post_content.'</p>
          <center>
          <div class="author clearfix">
          <div class="image" style="background-image:url('.wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail')[0].')"></div>
          <div class="by">
          <p class="name brand-primary-color">'.$meta['testimonial_name'][0].'</p>
          <p class="title">'.$meta['testimonial_title'][0].'</p>
          </div>
          </div>
          </center>
          </div>
          </div>
          </div>';
        }

    }

    wp_reset_query();

    $html .= '
    </div>
    </div>
    </div>
    </div>
    ';
    return $html;
  }

  // JS/CSS
  public function loadCssAndJs() {
    wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    wp_enqueue_style( 'vc_extend_style' );
    //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
  }

  /*
  Show notice if VC is not present
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }
}

// Initialize code
new VCTestimonials();
